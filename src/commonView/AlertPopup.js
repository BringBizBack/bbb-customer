import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Modal,
  SafeAreaView,
  TouchableWithoutFeedback,
  Image,
  Button,
} from 'react-native';

import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import COLOR from '../styles/Color';
import {CustomButton} from './CustomButton';
import IMAGES from '../styles/Images';
import Styles from '../styles/Styles';
import {K_ENGLISH_LANG, K_THAI_LANG} from './Constants';
import {Locale} from './Helpers';

export default AlertPopup = props => {
  return (
    <Modal
      animationType={'slide'}
      transparent={true}
      visible={props.modalVisible}
      onRequestClose={() => {
        props.closeVerifyModal();
      }}>
      <TouchableWithoutFeedback
        style={{flex: 1}}
        onPress={() => props.closeVerifyModal()}>
        <View style={styles.modal}>
          {props.isLanguagePopup == null && (
            <View style={styles.white_view}>
              <Image
                style={styles.icon_image}
                source={props.image ? props.image : IMAGES.questionmark}
              />
              <Text
                style={[
                  Styles.button_font,
                  {marginVertical: 10, fontWeight: '600'},
                ]}>
                {props.title ? props.title : 'Verified'}
              </Text>
              <Text style={[Styles.small_label, styles.small_label_style]}>
                {props.desc
                  ? props.desc
                  : 'The first time you redeem credits the business will verify that the number you entered for your ID card matches your actual ID card.\nWe do this to protect you account from identity theft.'}
              </Text>
              <View style={{width: '80%', marginTop: 20, marginBottom: 10}}>
                <CustomButton
                  onPress={() => {
                    props.closeVerifyModal();
                    if (props.prop) {
                      if (props.title != Locale('CREDIT REQUEST SENT')) {
                        props.prop.navigation.navigate('MyTabs', {index: 0});
                      }
                    }
                  }}
                  text={props.buttonText ? props.buttonText : 'I understand'}
                />
              </View>
            </View>
          )}
          {props.isLanguagePopup && (
            <View style={styles.white_view}>
              <Text
                style={[
                  Styles.button_font,
                  {marginVertical: 10, fontWeight: '600'},
                ]}>
                Select app language
              </Text>
              <Text style={[Styles.small_label, styles.small_label_style]}>
                You can change the language from settings.
              </Text>
              <View
                style={{
                  width: '80%',
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Button
                  title={'English'}
                  color={
                    props.selectedLanguage == K_ENGLISH_LANG
                      ? COLOR.DARK_BLUE
                      : COLOR.LIGHT_TEXT
                  }
                  onPress={() => {
                    props.closeVerifyModal(1);
                  }}
                />
                <Button
                  title={'Thailand'}
                  color={
                    props.selectedLanguage == K_THAI_LANG
                      ? COLOR.DARK_BLUE
                      : COLOR.LIGHT_TEXT
                  }
                  onPress={() => {
                    props.closeVerifyModal(2);
                  }}
                />
              </View>
              <View style={{width: '80%', marginTop: 20, marginBottom: 10}}>
                <CustomButton
                  onPress={() => {
                    props.closeVerifyModal(3);
                  }}
                  text={'Set as default language'}
                />
              </View>
            </View>
          )}
        </View>
      </TouchableWithoutFeedback>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#00000030',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    flex: 1,
  },
  icon_image: {
    height: 40,
    width: 40,
    marginTop: 10,
  },
  white_view: {
    width: wp(90),
    alignSelf: 'center',
    alignItems: 'center',
    backgroundColor: COLOR.WHITE,
    padding: 25,
    borderRadius: 20,
  },
  small_label_style: {
    marginVertical: 10,
    color: COLOR.BLACK,
    textAlign: 'center',
  },
});
