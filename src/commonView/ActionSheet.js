import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Modal,
  SafeAreaView,
  TextInput,
} from 'react-native';
import Styles from '../styles/Styles';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import COLOR from '../styles/Color';
import {CustomButton} from './CustomButton';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

export default ActionSheet = props => {
  return (
    <Modal
      animationType={'slide'}
      transparent={true}
      visible={props.modalVisible}
      onRequestClose={() => {
        console.log('Modal has been closed.');
      }}>
      <View style={styles.modal}>
        <SafeAreaView style={{flex: 1}}>
          <View style={styles.white_view}>
            <Text style={[Styles.subheading_label, {marginVertical: 10}]}>
              {props.isDispute ? 'Enter reason for dispute' : 'Choose Option'}
            </Text>
            {props.isDispute ? (
              <View
                style={{
                  flex: 1,
                  width: '90%',
                  alignSelf: 'center',
                }}>
                <View
                  style={{
                    flex: 1,
                    paddingHorizontal: 10,
                    borderWidth: 1,

                    // alignItems: 'flex-start',
                  }}>
                  <TextInput
                    multiline={true}
                    returnKeyLabel="Done"
                    returnKeyType="done"
                    onChangeText={val => props.onChange(val)}
                    style={[{width: '100%', height: 120, color: COLOR.BLACK}]}
                  />
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    flex: 1,
                    alignItems: 'center',
                  }}>
                  <View style={{flex: 1, marginVertical: 10, marginRight: 10}}>
                    <CustomButton
                      text={'Done'}
                      onPress={() => props.handleSheet(4)}
                    />
                  </View>
                  <View style={{flex: 1}}>
                    <CustomButton
                      text={'Cancel'}
                      bg={COLOR.RED}
                      onPress={() => props.handleSheet(2)}
                    />
                  </View>
                </View>
              </View>
            ) : (
              <View
                style={{
                  flex: 1,
                  width: '90%',
                }}>
                <CustomButton
                  text={'Open Camera'}
                  bg={COLOR.WHITE}
                  onPress={() => props.handleSheet(1)}
                />
                <CustomButton
                  text={'Open Gallery'}
                  bg={COLOR.WHITE}
                  onPress={() => props.handleSheet(2)}
                />

                <CustomButton
                  text={'Cancel'}
                  bg={COLOR.WHITE}
                  onPress={() => props.handleSheet(3)}
                />
              </View>
            )}
          </View>
        </SafeAreaView>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: '#00000030',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    flex: 1,
  },
  appicon_image: {
    height: 90,
    width: 90,
    marginTop: 50,
    marginLeft: 20,
    overflow: 'hidden',
  },
  white_view: {
    width: wp(90),
    alignSelf: 'center',
    alignItems: 'center',
    backgroundColor: COLOR.WHITE,
    bottom: 20,
    position: 'absolute',
    borderRadius: 20,
  },
});
