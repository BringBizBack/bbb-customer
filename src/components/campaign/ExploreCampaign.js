import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Image,
  TouchableWithoutFeedback,
  FlatList,
  RefreshControl,
  ActionSheetIOS,
  StyleSheet,
  Platform,
} from 'react-native';
import Styles from '../../styles/Styles';
import IMAGES from '../../styles/Images';
import NavigationBar from '../../commonView/NavigationBar';
import COLOR from '../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import ProgressCircle from 'react-native-progress-circle';
import {Dropdown} from '../../commonView/Dropdown';
import LinearGradient from 'react-native-linear-gradient';
import BottomView from '../../commonView/BottomView';
import {
  getDateFromTimeStamp,
  Locale,
  showAlertMessage,
} from '../../commonView/Helpers';
import API from '../../api/Api';
import database from '@react-native-firebase/database';
import {Pages} from 'react-native-pages';
import CampaignActions from '../../redux/action/CampaignActions';
import AuthenticationAction from '../../redux/action/AuthenticationAction';
import {connect} from 'react-redux';
import ActionSheet from 'react-native-actionsheet';

let api = new API();

class ExploreCampaign extends React.Component {
  constructor(props) {
    super();
    this.state = {
      campaignData: [],
      selectedIndex: 0,
      isTabbar: false,
      userDetail: null,
      refreshing: false,
      provinceData: null,
      selectedCity: null,
      selectedSort: null,
      redirectionFromLogin: false,
    };

    this.handleDropdown = this.handleDropdown.bind(this);
    this.handleSheetList = this.handleSheetList.bind(this);
  }

  componentDidMount() {
    this.handleRealtimeDB();
    this.props.getUserInfo();
    this.props.getCampaignData();
    this.getProvinceData();
    if (this.props.route.params) {
      if (this.props.route.params.tabScreen != null) {
        this.setState({
          isTabbar: this.props.route.params.tabScreen,
          redirectionFromLogin: this.props.route.params.redirectionFromLogin
            ? this.props.route.params.redirectionFromLogin
            : false,
        });
      }
    }
  }

  componentDidUpdate(
    prevProps: Readonly<P>,
    prevState: Readonly<S>,
    snapshot: SS,
  ) {
    if (prevProps.campaignData != this.props.campaignData) {
      this.setState({campaignData: this.props.campaignData});
    }
  }

  handleRealtimeDB() {
    let that = this;
    database()
      .ref('campaigns/')
      .on('value', snapshot => {
        setTimeout(function () {
          let campaign = that.state.campaignData;
          if (campaign && campaign.length && snapshot.hasChildren()) {
            snapshot.forEach((childSnapshot, i) => {
              campaign.forEach((val, index) => {
                if (val.id == childSnapshot.key) {
                  campaign[index].currentVisits = childSnapshot.val().visits;
                }
              });
            });
            that.setState({
              campaignData: campaign,
            });
          }
        }, 1000);
      });
  }

  getProvinceData() {
    api
      .getProvince(1)
      .then(json => {
        console.log('response is:-', json);
        this.setState({isLoading: false});
        if (json.status == 200) {
          let data = [];
          for (let i = 0; i < json.data.response.length + 1; i++) {
            if (i == 0) {
              data.push({
                id: null,
                label: 'All',
                value: 'All',
              });
            } else {
              if (i != 0) {
                data.push({
                  id: json.data.response[i - 1].id,
                  label: json.data.response[i - 1].province,
                  value: json.data.response[i - 1].province,
                });
              }
            }
          }

          this.setState({
            provinceData: data,
          });
        } else if (json.status == 400) {
          showAlertMessage(json.data.message, 2);
        } else {
          showAlertMessage(json.data.message, 2);
        }
      })
      .catch(error => {
        console.log('response is:-', error);
        this.setState({isLoading: false});
        showAlertMessage(error.response.data.message, 2);
      });
  }

  handleDropdown = val => {
    if (this.state.provinceData) {
      let selectedValue = this.state.provinceData.find(
        data => data.value === val,
      );
      if (selectedValue) {
        this.setState({
          selectedCity: selectedValue,
        });
        this.props.getCampaignData(
          1,
          selectedValue.id,
          this.state.selectedSort,
        );
      }
    }
  };

  noCampaignMessage() {
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 100,
          width: wp(90),
          alignSelf: 'center',
        }}>
        <Text
          style={[
            Styles.small_label,
            {color: COLOR.WHITE, textAlign: 'center'},
          ]}>
          {Locale('K_PULL_RESET')}
        </Text>
      </View>
    );
  }

  handleSheetList() {
    this.ActionSheet.show();
  }

  render() {
    const {isTabbar} = this.state;
    return (
      <View style={Styles.container}>
        <LinearGradient
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
          colors={[COLOR.TOP_GRADIENT, COLOR.BOTTOM_GRADIENT]}
          style={Styles.container}>
          <ActionSheet
            ref={o => (this.ActionSheet = o)}
            title={'Sort by'}
            options={[
              'All',
              'Sort by name A-Z',
              'Sort by name Z-A',
              'Start date - ASC',
              'Start date - DESC',
              'End date - ASC',
              'End date - DESC',
              'Cancel',
            ]}
            cancelButtonIndex={7}
            destructiveButtonIndex={7}
            onPress={buttonIndex => {
              if (buttonIndex === 7) {
              } else if (buttonIndex == 0) {
                this.props.getCampaignData(
                  1,
                  this.state.selectedCity ? this.state.selectedCity.id : null,
                  null,
                );
                this.setState({selectedSort: buttonIndex});
              } else {
                this.props.getCampaignData(
                  1,
                  this.state.selectedCity ? this.state.selectedCity.id : null,
                  buttonIndex,
                );
                this.setState({selectedSort: buttonIndex});
              }
            }}
          />
          <SafeAreaView style={Styles.container}>
            <NavigationBar title={'Campaigns'} />

            <View
              style={[
                style.dropdown_view,
                {
                  marginVertical: 15,
                },
              ]}>
              <View
                style={[
                  {
                    flex: 1,
                    marginRight: 5,
                    paddingVertical: 0,
                  },
                ]}>
                <Dropdown
                  placeholder={Locale('Nation')}
                  dropdown_style={{bottomBorderWidth: 1}}
                  value={'Thailand'}
                  items={[
                    {id: 0, label: 'Thailand', value: 'Thailand'},
                    //  {id: 1, label: 'Italy', value: 'Italy'},
                  ]}
                  zIndex={1000}
                  handleDropdown={this.handleDropdown}
                />
              </View>

              <View style={[{flex: 1, paddingVertical: 0, marginRight: 5}]}>
                <Dropdown
                  placeholder={Locale('City')}
                  type={1}
                  dropdown_style={{bottomBorderWidth: 1}}
                  items={this.state.provinceData ? this.state.provinceData : []}
                  value={
                    this.state.selectedCity
                      ? this.state.selectedCity.province
                      : ''
                  }
                  zIndex={5000}
                  handleDropdown={this.handleDropdown}
                />
              </View>
              <TouchableWithoutFeedback onPress={() => this.handleSheetList()}>
                <View
                  style={[
                    Styles.back_button,
                    {
                      backgroundColor: COLOR.YELLOW,
                      borderRadius: 10,
                      width: 45,
                    },
                  ]}>
                  <Text
                    style={[Styles.extra_small_label, {color: COLOR.WHITE}]}>
                    Sort
                  </Text>
                  <Image
                    source={IMAGES.down_arrow}
                    style={{height: 10, width: 10}}
                  />
                </View>
              </TouchableWithoutFeedback>
            </View>

            <FlatList
              style={style.flatlist_view}
              data={this.props.campaignData}
              renderItem={({item, index}) => (
                <View style={style.flatlist_main_view}>
                  <TouchableWithoutFeedback
                    onPress={() =>
                      this.props.navigation.navigate('CampaignDetail', {
                        tabScreen: isTabbar,
                        data: item,
                        redirectionFromLogin: this.state.redirectionFromLogin,
                      })
                    }>
                    <View
                      style={[Styles.shadow_view, style.flatlist_shadow_view]}>
                      <View style={{flex: 1}}>
                        <Pages
                          indicatorColor={'transparent'}
                          style={{
                            height: 180,
                            width: '100%',
                            flex: 1,
                          }}>
                          {item.trading &&
                            item.trading.images &&
                            item.trading.images.map(element => {
                              return (
                                <Image
                                  resizeMode={'cover'}
                                  style={{
                                    overflow: 'hidden',
                                    width: '100%',
                                    alignSelf: 'center',
                                    height: '100%',
                                    borderRadius: 5,
                                  }}
                                  source={{
                                    uri: element.image,
                                  }}
                                />
                              );
                            })}
                        </Pages>
                        <View
                          style={[
                            style.date_yellow_view,
                            {top: -10, position: 'absolute'},
                          ]}>
                          <Text
                            style={[
                              Styles.small_label,
                              {
                                marginHorizontal: 15,
                                color: COLOR.BLACK,
                              },
                            ]}>
                            {getDateFromTimeStamp(item.startDate, 'MMM DD') +
                              ' - ' +
                              getDateFromTimeStamp(
                                item.endDate,
                                'MMM DD, YYYY',
                              )}
                          </Text>
                        </View>
                        <View style={[style.flatlist_title_view]}>
                          <Text
                            style={[
                              Styles.bold_body_label,
                              {
                                marginHorizontal: 10,
                                color: COLOR.WHITE,
                              },
                            ]}>
                            {item.trading
                              ? item.trading.name
                              : 'Archies & Bear'}
                          </Text>
                          <Text
                            style={[
                              Styles.extra_small_label,
                              {
                                marginTop: 5,
                                marginHorizontal: 10,
                                color: COLOR.WHITE,
                              },
                            ]}>
                            {item.trading
                              ? item.trading.address
                              : 'New south whales, NY'}
                          </Text>
                        </View>

                        <View
                          style={{
                            margin: 10,
                            flexDirection: 'row',
                            alignItems: 'center',
                          }}>
                          <View
                            style={{
                              backgroundColor: 'rgba(6, 201, 2, 0.20)',
                              borderRadius: 40,
                              marginRight: 10,
                            }}>
                            <ProgressCircle
                              percent={parseInt(item.currentStatus ?? '0')}
                              radius={28}
                              borderWidth={5}
                              color={COLOR.YELLOW}
                              shadowColor={COLOR.DARK_BLUE}
                              bgColor={COLOR.DARK_BLUE}>
                              <Text
                                style={[
                                  Styles.button_font,
                                  {color: COLOR.WHITE},
                                ]}>
                                {item.currentStatus ?? '0%'}
                              </Text>
                            </ProgressCircle>
                          </View>
                          <View style={{flex: 1}}>
                            <Text
                              style={[
                                Styles.bold_body_label,
                                {
                                  margin: 0,

                                  fontWeight: '600',
                                  fontSize: 15,
                                  alignSelf: 'flex-start',
                                },
                              ]}>
                              {item.campaignTemplate
                                ? item.campaignTemplate.name
                                : 'Win what you spend'}
                            </Text>
                            <View
                              style={{
                                flexDirection: 'row',
                                marginTop: 10,
                                flex: 1,
                              }}>
                              <View
                                style={{
                                  flexDirection: 'row',
                                  alignItems: 'center',
                                  margin: 0,
                                  marginLeft: 0,
                                  flex: 1,
                                }}>
                                <Image
                                  style={{
                                    height: 20,
                                    width: 20,
                                    marginRight: 10,
                                    tintColor: COLOR.DARK_BLUE,
                                  }}
                                  resizeMode={'cover'}
                                  source={IMAGES.dart_board}
                                />
                                <View style={{margin: 0}}>
                                  <Text
                                    style={[
                                      Styles.small_label,
                                      {color: COLOR.LIGHT_TEXT, margin: 0},
                                    ]}>
                                    Goal
                                  </Text>
                                  <Text
                                    style={[
                                      Styles.bold_body_label,
                                      {margin: 0},
                                    ]}>
                                    {item.goal} visits
                                  </Text>
                                </View>
                              </View>
                              <View
                                style={{
                                  flexDirection: 'row',
                                  alignItems: 'center',
                                  margin: 0,
                                  flex: 1,
                                }}>
                                <Image
                                  style={{
                                    height: 20,
                                    width: 20,
                                    marginRight: 10,
                                  }}
                                  resizeMode={'cover'}
                                  source={IMAGES.user}
                                />
                                <View style={{margin: 0}}>
                                  <Text
                                    style={[
                                      Styles.small_label,
                                      {color: COLOR.LIGHT_TEXT, margin: 0},
                                    ]}>
                                    Current
                                  </Text>
                                  <Text
                                    style={[
                                      Styles.bold_body_label,
                                      {margin: 0},
                                    ]}>
                                    {item.currentVisits} visits
                                  </Text>
                                </View>
                              </View>
                            </View>
                          </View>
                        </View>
                      </View>
                    </View>
                  </TouchableWithoutFeedback>
                </View>
              )}
              showsVerticalScrollIndicator={false}
              showsHorizontalScrollIndicator={false}
              keyExtractor={(item, index) => index.toString()}
              ListHeaderComponent={
                this.props.campaignData && this.props.campaignData.length > 0
                  ? null
                  : this.noCampaignMessage()
              }
              refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={() => {
                    this.props.getCampaignData(1,this.state.selectedCity ? this.state.selectedCity.id:null, this.state.selectedSort);
                    this.setState({
                      refreshing: false,
                    });
                  }}
                />
              }
            />

            {this.state.redirectionFromLogin ? <BottomView /> : null}
          </SafeAreaView>
        </LinearGradient>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadowView: {
    backgroundColor: COLOR.LIGHT_BLUE,
    borderRadius: 5,
    paddingVertical: 15,
    paddingHorizontal: 10,
  },
  flatlist_view: {
    marginVertical: 0,
    marginTop: 15,
    width: wp(100),
    alignSelf: 'center',
  },
  flatlist_main_view: {
    justifyContent: 'center',
    alignItems: 'center',
    width: wp(90),
    alignSelf: 'center',
  },
  flatlist_title_view: {
    position: 'absolute',
    bottom: Platform.OS == 'android' ? 100 : 90,
    width: '100%',
    backgroundColor: '#00000050',
    paddingVertical: 5,
  },
  date_yellow_view: {
    height: 25,
    borderTopRightRadius: 12.5,
    borderBottomRightRadius: 12.5,
    marginTop: 20,
    marginLeft: 0,
    backgroundColor: COLOR.YELLOW,
    justifyContent: 'center',
    alignSelf: 'flex-start',
  },
  heading_label: {
    color: COLOR.WHITE,
    width: wp(90),
    alignSelf: 'center',
    marginVertical: 10,
    paddingLeft: 45,
  },
  dropdown_view: {
    width: wp(90),
    alignSelf: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 5,
    ...(Platform.OS === 'ios' && {
      zIndex: 1000,
    }),
  },
  text_horizontal: {
    transform: [{rotate: '270deg'}, {translateX: 0}, {translateY: 0}],
    width: 270,
    height: 24,
    textAlign: 'center',
  },
  horizontal_text_view: {
    width: 30,
    height: 270,
    alignItems: 'center',
    justifyContent: 'center',
  },
  flatlist_shadow_view: {
    width: wp(90),
    borderRadius: 5,
    overflow: 'hidden',
    backgroundColor: COLOR.WHITE,
    marginBottom: 15,
    alignSelf: 'center',
    flexDirection: 'row',
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    userDetail: state.authReducer.userInfo,
    campaignData: state.campaignReducer.campaignData,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getUserInfo: val => {
      dispatch(AuthenticationAction.getUserDetail(val));
    },
    getCampaignData: (val, id, sort) => {
      dispatch(CampaignActions.getCampaignData(val, id, sort));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ExploreCampaign);
