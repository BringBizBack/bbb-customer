import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  Image,
  TouchableWithoutFeedback,
} from 'react-native';
import Styles from '../../styles/Styles';
import IMAGES from '../../styles/Images';
import NavigationBar from '../../commonView/NavigationBar';
import COLOR from '../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {CustomButton} from '../../commonView/CustomButton';
import LinearGradient from 'react-native-linear-gradient';
import {MobileView} from './TopWhiteView';
import AlertPopup from '../../commonView/AlertPopup';
import {
  getDateFromTimeStamp,
  insertComma,
  Locale,
  navigate,
  showAlertMessage,
} from '../../commonView/Helpers';
import ActivityIndicator from '../../commonView/ActivityIndicator';
import API from '../../api/Api';
import AuthenticationAction from '../../redux/action/AuthenticationAction';
import {connect} from 'react-redux';
import CampaignActions from '../../redux/action/CampaignActions';
import BottomView from '../../commonView/BottomView';

let api = new API();

class AddCampaignDetail extends React.Component {
  constructor() {
    super();
    this.state = {
      alertModalVisible: false,
      campaignData: null,
      termsTick: false,
      isLoading: false,
      redirectionFromLogin: false,
      templateData: null,
    };

    this.handleSubmitButton = this.handleSubmitButton.bind(this);
  }

  componentDidMount() {
    if (this.props.route.params) {
      if (this.props.route.params.data != null) {
        this.setState({
          campaignData: this.props.route.params.data,

          templateData: this.props.route.params.templateData,
          redirectionFromLogin: this.props.route.params.redirectionFromLogin
            ? this.props.route.params.redirectionFromLogin
            : false,
        });
      }
    }
  }

  closeVerifyModal = () => {
    this.setState({alertModalVisible: false});
  };

  handleSubmitButton = () => {
    if (this.state.termsTick == false) {
      showAlertMessage(Locale('Please agree our terms & condition'));
    } else {
      let that = this;
      let campaign = this.state.campaignData;
      let data = JSON.stringify({
        trading: this.props.userBusinessDetail.tradingInfo.id,
        campaignTemplate: this.state.templateData.id,
        maxPrize: campaign.prizeLimit,
        startDate: campaign.startDate,
        endDate: campaign.endDate,
        goal: campaign.goal,
        startTime: campaign.startTime,
        endTime: campaign.endTime,
      });

      that.setState({isLoading: true});
      api
        .addCamapaign(data)
        .then(json => {
          console.log('campaign response is:-', json);
          that.setState({isLoading: false});
          if (json.status == 200) {
            this.props.getCampaignData(1, 1);
            this.setState({alertModalVisible: true});
          } else if (json.status == 400) {
            showAlertMessage(json.data.message, 2);
          } else {
            showAlertMessage(json.data.message, 2);
          }
        })
        .catch(error => {
          that.setState({isLoading: false});
          if (error.response.data) {
            showAlertMessage(error.response.data.message, 2);
          }
        });
    }
  };

  closeVerifyModal = () => {
    this.setState({alertModalVisible: false}, () => {
      this.props.navigation.navigate('MyTabs');
    });
  };

  campaignDetail = props => {
    return (
      <View style={{marginBottom: 15}}>
        <Text style={[Styles.extra_small_label, {color: COLOR.WHITE}]}>
          {props.title}
        </Text>
        <Text style={[Styles.bold_body_label, {color: COLOR.WHITE}]}>
          {props.detail}
        </Text>
      </View>
    );
  };

  getBusinessStatus = props => {
    if (props) {
      switch (props.campaignStatus) {
        case 1:
          return 'Waiting';
          break;
        case 2:
          return 'Approved';
          break;
        case 3:
          return 'Rejected';
          break;
        default:
          return 'N/A';
          break;
      }
    } else {
      return 'Add Business';
    }
  };

  render() {
    let {campaignData, isLoading} = this.state;
    return (
      <View style={Styles.container}>
        <LinearGradient
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
          colors={[COLOR.TOP_GRADIENT, COLOR.BOTTOM_GRADIENT]}
          style={Styles.container}>
          <AlertPopup
            image={IMAGES.security}
            title={Locale('THANK YOU')}
            buttonText={Locale('Home')}
            desc={Locale('K_GOALPRIZE_SUBMITTED')}
            closeVerifyModal={() => this.closeVerifyModal()}
            modalVisible={this.state.alertModalVisible}
          />
          <ActivityIndicator loading={isLoading} />
          <AlertPopup
            image={IMAGES.security}
            title={Locale('THANK YOU')}
            buttonText={Locale('Ok')}
            desc={Locale('K_GOALPRIZE_SUBMITTED')}
            closeVerifyModal={() => this.closeVerifyModal()}
            modalVisible={this.state.alertModalVisible}
          />
          <SafeAreaView style={Styles.container}>
            <NavigationBar title={Locale('Campaign Details')} />

            <ScrollView>
              <View style={[Styles.shadow_view, style.shadowView]}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}>
                  <View style={[Styles.left_corner_view, {marginVertical: 15}]}>
                    <Text style={[Styles.extra_small_label]}>
                      {Locale('Details')}
                    </Text>
                  </View>
                </View>
                <View style={{paddingHorizontal: 15, alignItems: 'center'}}>
                  <View style={{width: '100%'}}>
                    <this.campaignDetail
                      title={Locale('Starts')}
                      detail={
                        campaignData &&
                        getDateFromTimeStamp(
                          campaignData.startDate
                            ? campaignData.startDate
                            : '1614567890',
                          'MMM DD, YYYY',
                        ) +
                          ' ' +
                          getDateFromTimeStamp(
                            campaignData.startTime
                              ? campaignData.startTime
                              : '1614567890',
                            'h:mm a',
                          )
                      }
                    />
                    <this.campaignDetail
                      title={Locale('Ends')}
                      detail={
                        campaignData &&
                        getDateFromTimeStamp(
                          campaignData.endDate
                            ? campaignData.endDate
                            : '1614567890',
                          'MMM DD, YYYY',
                        ) +
                          ' ' +
                          getDateFromTimeStamp(
                            campaignData.endTime
                              ? campaignData.endTime
                              : '1614567890',
                            'h:mm a',
                          )
                      }
                    />
                    <View style={{flexDirection: 'row'}}>
                      <View style={{flex: 1, marginRight: 10}}>
                        <this.campaignDetail
                          title={Locale('Goal')}
                          detail={
                            campaignData
                              ? campaignData.goal + Locale(' customer visits')
                              : '1614567890'
                          }
                        />
                      </View>
                      <TouchableWithoutFeedback
                        onPress={() => {
                          showAlertMessage(Locale('Goals targeted'));
                        }}>
                        <Image
                          style={{height: 15, width: 15}}
                          resizeMode={'contain'}
                          source={IMAGES.info}
                        />
                      </TouchableWithoutFeedback>
                    </View>
                    <this.campaignDetail
                      title={Locale('Minimum spend')}
                      detail={
                        campaignData
                          ? insertComma(
                              campaignData.minSpent ??
                                campaignData.sliderTwo ??
                                '2000',
                            ) + ' THB'
                          : '2000'
                      }
                    />
                    <View style={{flexDirection: 'row'}}>
                      <View style={{flex: 1, marginRight: 10}}>
                        <this.campaignDetail
                          title={Locale('Winners')}
                          detail={`Top ${
                            campaignData
                              ? campaignData.winners ??
                                campaignData.sliderOne ??
                                '20'
                              : '20'
                          }% of participating Spenders if campaign hits goal before end.`}
                        />
                      </View>
                      <TouchableWithoutFeedback
                        onPress={() => {
                          showAlertMessage(Locale('Participating spendors'));
                        }}>
                        <Image
                          style={{height: 15, width: 15}}
                          resizeMode={'contain'}
                          source={IMAGES.info}
                        />
                      </TouchableWithoutFeedback>
                    </View>
                    <this.campaignDetail
                      title={Locale('Prize')}
                      detail={`${
                        campaignData
                          ? campaignData.campaignTemplate
                            ? campaignData.campaignTemplate.name
                            : this.state.templateData
                            ? this.state.templateData.name
                            : Locale('Win what you spent')
                          : Locale('Win what you spent')
                      }\nDigital gift card with ${
                        campaignData
                          ? campaignData.campaignTemplate
                            ? campaignData.campaignTemplate.pricePercentage ??
                              campaignData.sliderTwo
                            : campaignData.sliderTwo ?? '20'
                          : ''
                      }% of what you spent.`}
                    />
                    <this.campaignDetail
                      title={Locale('Maximum Prize Amount')}
                      detail={
                        campaignData
                          ? insertComma(
                              campaignData.limit ??
                                campaignData.prizeLimit ??
                                '20000',
                            ) + ' THB'
                          : ''
                      }
                    />
                    <this.campaignDetail
                      title={Locale('Credit expiration')}
                      detail={`${
                        campaignData
                          ? campaignData.campaignTemplate
                            ? campaignData.campaignTemplate.creditExpiry
                            : this.state.templateData
                            ? this.state.templateData.creditExpiry
                            : '120'
                          : ''
                      } days after campaign starts`}
                    />
                  </View>
                </View>
              </View>
            </ScrollView>
            {this.state.redirectionFromLogin ? <BottomView /> : null}
          </SafeAreaView>
        </LinearGradient>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadowView: {
    backgroundColor: COLOR.LIGHT_BLUE,
    borderRadius: 5,
    width: wp(90),
    alignSelf: 'center',
    marginTop: 15,
  },
  bottomButton: {marginVertical: 20, width: wp(90), alignSelf: 'center'},
  titleLabel: {color: COLOR.WHITE, width: 120, margin: 0, fontSize: 13},
  icon_image: {
    height: 40,
    width: 40,
  },
  purple_view: {
    backgroundColor: '#BA9AFF',
    margin: 10,
    padding: 15,
    borderRadius: 10,
  },
});

const mapDispatchToProps = dispatch => {
  return {
    getCampaignData: val => {
      dispatch(CampaignActions.getCampaignData(1, 1));
    },
  };
};

export default connect(null, mapDispatchToProps)(AddCampaignDetail);
