import React, {Component} from 'react';
import {
  View,
  ImageBackground,
  SafeAreaView,
  StyleSheet,
  Image,
  TouchableWithoutFeedback,
  ScrollView,
  Text,
} from 'react-native';
import Styles from '../../styles/Styles';
import IMAGES from '../../styles/Images';
import NavigationBar from '../../commonView/NavigationBar';
import COLOR from '../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import FONTS from '../../styles/Fonts';
import LinearGradient from 'react-native-linear-gradient';
import BottomView from '../../commonView/BottomView';
import {Locale} from '../../commonView/Helpers';

export default class CampaignRules extends React.Component {
  constructor() {
    super();
    this.state = {
      isTabbar: false,
    };
    this.updateMasterState = this.updateMasterState.bind(this);
  }

  componentDidMount(props) {
    if (this.props.route.params) {
      if (this.props.route.params.tabScreen != null) {
        this.setState({
          isTabbar: this.props.route.params.tabScreen,
        });
      }
    }
  }

  updateMasterState = props => {
    // this.setState({
    //     [props.name]:props.value
    // })
  };

  commonView = props => {
    return (
      <View>
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <Image
            source={props.image}
            style={{height: 25, width: 25}}
            resizeMode={'cover'}
          />
          <View style={{flex: 1, marginHorizontal: 15}}>
            <Text
              style={[
                Styles.body_label,
                {
                  color: COLOR.BLACK,
                  fontFamily: FONTS.FAMILY_MEDIUM,
                },
              ]}>
              {props.heading}
            </Text>
            <Text
              style={[
                Styles.extra_small_label,
                {color: COLOR.LIGHT_TEXT, marginTop: 5},
              ]}>
              {props.description}
            </Text>
          </View>
          <TouchableWithoutFeedback>
            <Text
              style={[
                Styles.extra_small_label,
                {
                  color: COLOR.LIGHT_BLUE,

                  textDecorationLine: 'underline',
                  width: 90,
                  textAlign: 'right',
                },
              ]}>
              {props.buttonLabel}
            </Text>
          </TouchableWithoutFeedback>
        </View>
        <View
          style={[
            Styles.line_view,
            {
              marginVertical: 15,
            },
          ]}
        />
      </View>
    );
  };

  render() {
    const {isTabbar} = this.state;
    return (
      <View style={Styles.container}>
        <LinearGradient
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
          colors={[COLOR.TOP_GRADIENT, COLOR.BOTTOM_GRADIENT]}
          style={Styles.container}>
          <SafeAreaView style={Styles.container}>
            <NavigationBar />
            <ScrollView>
              <ImageBackground
                resizemode={'cover'}
                source={IMAGES.main_banner}
                style={[
                  Styles.shadow_view,
                  {
                    height: 180,
                    marginBottom: 0,
                    marginTop: 15,
                    width: wp(90),
                    alignSelf: 'center',
                    borderRadius: 5,
                    overflow: 'hidden',
                  },
                ]}>
                <View style={style.date_yellow_view}>
                  <Text style={[Styles.small_label, {marginHorizontal: 15}]}>
                    May 5 - May 28, 2021
                  </Text>
                </View>
                <View style={style.archies_view}>
                  <Text
                    style={[
                      Styles.button_font,
                      {
                        marginHorizontal: 10,
                        color: COLOR.WHITE,
                        alignSelf: 'flex-start',
                      },
                    ]}>
                    Archie's bar $ grill
                  </Text>
                </View>
              </ImageBackground>
              <View style={[style.shadowView, Styles.shadow_view]}>
                <View style={style.rule_view}>
                  <Image
                    source={IMAGES.trophy}
                    style={{height: 35, width: 35}}
                    resizeMode={'cover'}
                  />
                  <View style={{flex: 1, marginHorizontal: 15}}>
                    <Text style={[Styles.subheading_label]}>
                      {Locale('Rules of campaign')}
                    </Text>
                  </View>
                </View>

                <Text
                  style={[
                    Styles.small_label,
                    {color: COLOR.BLACK, marginTop: 5},
                  ]}>
                  This is a legally binding document establishing the Rules of
                  the Campaign which both the Business and d Customer show they
                  agree through their participation in a Campaign.{'\n'}A
                  Goalprize campaign is an attempt by a Business to increase the
                  number of customers in a specified time period.{'\n'}The
                  Business hosting the campaign will give out Rewards only if it
                  achieves its Goal before the Deadline. If it does not achieve
                  this Goal before the Deadline no Rewards will be given out.
                  {'\n'}The Goal is the number of paying customers that visits
                  the restaurant spending more than the minimum spending amount.
                  {'\n'}The Rewards are digital gift cards equal to the amount
                  spent during the Campaign time period by the winning customers
                  limited as indicated in the Campaign information. Credits on
                  the digital gift cards are given and redeemed using the BBB
                  mobile website in the restaurant not redeemable for delivery
                  service.{'\n'}The Winners are the customers that spend the
                  most during the Campaign . The number of winners is indicated
                  in the Campaign information.
                </Text>
              </View>
            </ScrollView>
            {!isTabbar && <BottomView />}
          </SafeAreaView>
        </LinearGradient>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadowView: {
    backgroundColor: COLOR.WHITE,
    borderRadius: 5,
    width: wp(90),
    alignSelf: 'center',
    marginVertical: 20,
    padding: 15,
  },
  date_yellow_view: {
    height: 25,
    borderTopRightRadius: 15,
    borderBottomRightRadius: 15,
    marginTop: 20,
    marginLeft: 0,
    backgroundColor: COLOR.YELLOW,
    justifyContent: 'center',
    alignSelf: 'flex-start',
  },
  rule_view: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 5,
  },
  archies_view: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    backgroundColor: '#00000050',
    paddingVertical: 5,
  },
});
