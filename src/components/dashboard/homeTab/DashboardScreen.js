import React, {Component} from 'react';
import {
  View,
  SafeAreaView,
  StyleSheet,
  Image,
  TouchableWithoutFeedback,
  ScrollView,
  Text,
  FlatList,
  RefreshControl,
} from 'react-native';
import Styles from '../../../styles/Styles';
import IMAGES from '../../../styles/Images';
import COLOR from '../../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {CustomButton} from '../../../commonView/CustomButton';
import LinearGradient from 'react-native-linear-gradient';
import AuthenticationAction from '../../../redux/action/AuthenticationAction';
import {connect} from 'react-redux';
import FONTS from '../../../styles/Fonts';
import {
  getDateFromTimeStamp,
  handlingGPNumber,
  insertComma,
  Locale,
  showAlertMessage,
} from '../../../commonView/Helpers';
import CampaignActions from '../../../redux/action/CampaignActions';

class DashboardScreen extends React.Component {
  constructor(props) {
    super();
    this.state = {
      isTabbar: false,
      refreshing: false,
    };
    this.updateMasterState = this.updateMasterState.bind(this);
    this.handleRedeemCredit = this.handleRedeemCredit.bind(this);
    this.noCampaignMessage = this.noCampaignMessage.bind(this);
  }

  componentDidMount(props) {
    this.props.getUserDetail();
    this.props.getMyWonGoalprize();
    this.props.getMyParticipatedCampaign();
    this.props.getLostCampaignData();

    if (this.props.route.params) {
      if (this.props.route.params.tabScreen != null) {
        this.setState({
          isTabbar: this.props.route.params.tabScreen,
        });
      }
    }
  }

  updateMasterState = props => {};

  handleRedirection = id => {
    switch (id) {
      case 1:
        this.props.navigation.navigate('CampaignRules');
        break;
      case 2:
        this.props.navigation.navigate('SettingsScreen');
        break;
      case 3:
        this.props.navigation.navigate('CampaignRules', {tabScreen: true});
        break;
      case 4:
        this.props.navigation.navigate('AboutScreen', {tabScreen: true});
        break;
      default:
        break;
    }
  };

  handleRedeemCredit = () => {
    this.props.navigation.navigate('RedeemCreditScreen');
  };

  campaignView = props => {
    return (
      <View>
        <TouchableWithoutFeedback
          onPress={() => {
            this.handleRedirection(props.id);
          }}>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              alignItems: 'center',
              paddingHorizontal: 5,
            }}>
            <Image
              source={props.image}
              style={{height: 35, width: 35}}
              resizeMode={'contain'}
            />
            <View style={{flex: 1, marginHorizontal: 15}}>
              <Text style={[Styles.bold_body_label, {color: COLOR.WHITE}]}>
                {props.title}
              </Text>
            </View>
            <Image
              source={IMAGES.keyboard_arrow_right}
              style={{height: 12, width: 12, tintColor: COLOR.WHITE}}
              resizeMode={'contain'}
            />
          </View>
        </TouchableWithoutFeedback>
        {props.showLine && (
          <View
            style={[
              Styles.line_view,
              {
                borderBottomWidth: 0.4,
                marginVertical: 15,
                borderBottomColor: COLOR.DARK_BLUE,
              },
            ]}
          />
        )}
      </View>
    );
  };

  noCampaignMessage(msg) {
    return (
      <View
        style={{
          alignItems: 'flex-start',
          marginHorizontal: 10,
          width: wp(85),
          alignSelf: 'center',
        }}>
        <Text
          style={[
            Styles.small_label,
            {
              color: COLOR.WHITE,
              textAlign: 'left',
              multiline: true,
              margin: 0,
            },
          ]}>
          {msg}
        </Text>
      </View>
    );
  }

  render() {
    const {isTabbar} = this.state;
    return (
      <View style={Styles.container}>
        <LinearGradient
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
          colors={[COLOR.TOP_GRADIENT, COLOR.BOTTOM_GRADIENT]}
          style={Styles.container}>
          <SafeAreaView style={Styles.container}>
            <View
              style={{
                width: wp(90),
                alignSelf: 'center',
                marginTop: 10,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Image
                source={IMAGES.logo}
                style={{height: 45, width: 140, marginLeft: 0}}
                resizeMode={'cover'}
              />
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <TouchableWithoutFeedback
                  onPress={() =>
                    this.props.navigation.navigate('NotificationScreen')
                  }>
                  <View>
                    <Image
                      source={IMAGES.alarm}
                      style={{
                        height: 30,
                        width: 30,
                        borderRadius: 20,
                        marginRight: 10,
                        tintColor: COLOR.WHITE,
                      }}
                      resizeMode={'contain'}
                    />
                  </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback
                  onPress={() =>
                    this.props.navigation.navigate('SettingsScreen')
                  }>
                  <Image
                    source={
                      this.props.userDetail
                        ? {uri: this.props.userDetail.profile}
                        : ''
                    }
                    style={{height: 40, width: 40, borderRadius: 20}}
                    resizeMode={'cover'}
                  />
                </TouchableWithoutFeedback>
              </View>
            </View>
            <ScrollView
              refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={() => {
                    this.setState({
                      refreshing: false,
                    });
                    this.props.getUserDetail(1);
                    this.props.getMyWonGoalprize(1);
                    this.props.getMyParticipatedCampaign(1);
                    this.props.getLostCampaignData(1);
                  }}
                />
              }>
              <Text
                style={[
                  Styles.button_font,
                  {
                    color: COLOR.WHITE,
                    marginTop: 15,
                    width: wp(90),
                    alignSelf: 'center',
                  },
                ]}>
                GoalPrize ID #{' '}
                {this.props.userDetail
                  ? handlingGPNumber(this.props.userDetail.bbb_id)
                  : ''}
              </Text>
              {/*<View*/}
              {/*  style={[*/}
              {/*    style.shadowView,*/}
              {/*    Styles.shadow_view,*/}
              {/*    {borderRadius: 5, backgroundColor: COLOR.MEDIUM_BLUE},*/}
              {/*  ]}>*/}
              {/*  <this.campaignView*/}
              {/*    id={3}*/}
              {/*    showLine={true}*/}
              {/*    image={IMAGES.smartphone}*/}
              {/*    title={'GoalPrize: How to win'}*/}
              {/*  />*/}
              {/*  <this.campaignView*/}
              {/*    id={4}*/}
              {/*    showLine={false}*/}
              {/*    image={IMAGES.trophy}*/}
              {/*    title={'GoalPrize: Where to find them'}*/}
              {/*  />*/}
              {/*</View>*/}

              <View
                style={[
                  style.shadowView,
                  Styles.shadow_view,
                  {marginBottom: 15, marginTop: 15},
                ]}>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}>
                  <View style={style.date_yellow_view}>
                    <Text
                      style={[
                        Styles.bold_body_label,
                        {marginHorizontal: 15, fontSize: 10},
                      ]}>
                      {Locale('GoalPrize: I currently participated in')}
                    </Text>
                  </View>
                </View>
                <FlatList
                  style={[{marginTop: 20}]}
                  data={this.props.participatedCampaignData}
                  renderItem={({item, index}) => (
                    <TouchableWithoutFeedback
                      onPress={() =>
                        this.props.navigation.navigate('CampaignDetail', {
                          tabScreen: true,
                          data: item,
                          redrectionFromLogin: false,
                        })
                      }>
                      <View
                        style={[
                          Styles.shadow_view,
                          {
                            width: wp(45),
                            borderRadius: 5,
                            overflow: 'hidden',
                            backgroundColor: COLOR.DARK_BLUE,
                            marginLeft: 10,
                          },
                        ]}>
                        <View
                          style={{
                            margin: 0,
                            padding: 10,
                          }}>
                          <View
                            style={{
                              flexDirection: 'row',
                              justifyContent: 'space-between',
                              margin: 0,
                            }}>
                            <Text
                              style={[Styles.body_label, style.campaign_view]}>
                              {item.campaignTemplate
                                ? item.campaignTemplate.name
                                : item.trading.name}
                            </Text>
                            <Image
                              source={IMAGES.mono}
                              style={{
                                height: 30,
                                width: 30,
                                marginLeft: 5,
                              }}
                              resizeMode={'contain'}
                            />
                          </View>
                          <View
                            style={{
                              flexDirection: 'row',
                              flex: 1,
                              marginTop: 10,
                            }}>
                            <View style={{flex: 1, marginRight: 10}}>
                              <Text
                                style={[
                                  Styles.extra_small_label,
                                  {margin: 0, marginTop: 5, color: COLOR.WHITE},
                                ]}>
                                {Locale('My total spent')}
                              </Text>
                              <Text
                                style={[
                                  Styles.small_label,
                                  {
                                    margin: 0,
                                    marginTop: 5,
                                    color: COLOR.WHITE,
                                    fontWeight: '600',
                                  },
                                ]}>
                                {insertComma(item.totalCredits)} THB
                              </Text>
                            </View>
                            <View style={{width: 60}}>
                              <Text
                                style={[
                                  Styles.extra_small_label,
                                  {margin: 0, marginTop: 5, color: COLOR.WHITE},
                                ]}>
                                {Locale('My rank')}
                              </Text>
                              <Text
                                style={[
                                  Styles.small_label,
                                  {
                                    margin: 0,
                                    marginTop: 3,
                                    color: COLOR.WHITE,
                                    fontWeight: '600',
                                  },
                                ]}>
                                {item.rank}
                              </Text>
                            </View>
                          </View>
                        </View>
                      </View>
                    </TouchableWithoutFeedback>
                  )}
                  ListHeaderComponent={
                    this.props.participatedCampaignData
                      ? this.props.participatedCampaignData.length > 0
                        ? null
                        : this.noCampaignMessage(
                            Locale(
                              "You haven't participated in any campaign(s) yet",
                            ),
                          )
                      : this.noCampaignMessage(
                          Locale(
                            "You haven't participated in any campaign(s) yet",
                          ),
                        )
                  }
                  horizontal={true}
                  showsVerticalScrollIndicator={false}
                  showsHorizontalScrollIndicator={false}
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>

              <View
                style={[
                  style.shadowView,
                  Styles.shadow_view,
                  {marginVertical: 0},
                ]}>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}>
                  <View style={style.date_yellow_view}>
                    <Text
                      style={[
                        Styles.bold_body_label,
                        {marginHorizontal: 15, fontSize: 10},
                      ]}>
                      {Locale('Digital gift cards I won from GoalPrize')}
                    </Text>
                  </View>
                </View>
                <FlatList
                  style={[{marginTop: 20}]}
                  data={this.props.wonGoalprizeData}
                  renderItem={({item, index}) => (
                    <TouchableWithoutFeedback
                      onPress={() => {
                        this.props.navigation.navigate('RedeemCreditScreen', {
                          tabScreen: true,
                          data: item,
                          type: true,
                        });
                      }}>
                      <View
                        style={[
                          Styles.shadow_view,
                          {
                            width: wp(55),
                            borderRadius: 5,
                            overflow: 'hidden',
                            backgroundColor: '#FC8686',
                            marginLeft: 10,
                          },
                        ]}>
                        <View
                          style={{
                            margin: 0,
                            padding: 10,
                          }}>
                          <View
                            style={{
                              flexDirection: 'row',
                              justifyContent: 'space-between',
                              margin: 0,
                            }}>
                            <Image
                              source={{uri: item.trading.logo}}
                              style={{
                                height: 40,
                                width: 40,
                                borderRadius: 20,
                                marginRight: 5,
                              }}
                              resizeMode={'cover'}
                            />
                            <View style={{flex: 1}}>
                              <Text
                                style={[
                                  Styles.bold_body_label,
                                  style.campaign_view,
                                ]}>
                                {item.trading.name}
                              </Text>
                              <Text
                                style={[
                                  Styles.small_label,
                                  {
                                    marginTop: 5,
                                    fontFamily: FONTS.FAMILY_SEMIBOLD,
                                    color: COLOR.WHITE,
                                  },
                                ]}>
                                {item.campaign
                                  ? item.campaign.name
                                  : item.campaignTemplate
                                  ? item.campaignTemplate.name
                                  : ''}
                              </Text>
                              <View
                                style={{
                                  flexDirection: 'row',
                                  flex: 1,
                                  marginTop: 10,
                                }}>
                                <View style={{flex: 1, marginRight: 10}}>
                                  <Text
                                    style={[
                                      Styles.extra_small_label,
                                      {
                                        margin: 0,
                                        marginTop: 5,
                                        color: COLOR.WHITE,
                                      },
                                    ]}>
                                    {Locale('Balance')}
                                  </Text>
                                  <Text
                                    style={[
                                      Styles.small_label,
                                      {
                                        margin: 0,
                                        marginTop: 5,
                                        color: COLOR.WHITE,
                                        fontWeight: '600',
                                      },
                                    ]}>
                                    {insertComma(item.totalCredits)} THB
                                  </Text>
                                </View>
                                <View style={{width: 65}}>
                                  <Text
                                    style={[
                                      Styles.extra_small_label,
                                      {
                                        margin: 0,
                                        marginTop: 5,
                                        color: COLOR.WHITE,
                                      },
                                    ]}>
                                    {Locale('Expires')}
                                  </Text>
                                  <Text
                                    style={[
                                      Styles.small_label,
                                      {
                                        margin: 0,
                                        marginTop: 3,
                                        color: COLOR.WHITE,
                                        fontWeight: '600',
                                      },
                                    ]}>
                                    {getDateFromTimeStamp(
                                      item.expiry,
                                      'MMM DD, YYYY',
                                    )}
                                  </Text>
                                </View>
                              </View>
                            </View>
                          </View>
                        </View>
                      </View>
                    </TouchableWithoutFeedback>
                  )}
                  horizontal={true}
                  ListHeaderComponent={
                    this.props.wonGoalprizeData
                      ? this.props.wonGoalprizeData.length > 0
                        ? null
                        : this.noCampaignMessage(
                            Locale("You haven't won any campaign(s) yet"),
                          )
                      : this.noCampaignMessage(
                          Locale("You haven't won any campaign(s) yet"),
                        )
                  }
                  showsVerticalScrollIndicator={false}
                  showsHorizontalScrollIndicator={false}
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>
              <View
                style={[
                  style.shadowView,
                  Styles.shadow_view,
                  {marginVertical: 0, marginTop: 15},
                ]}>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}>
                  <View style={style.date_yellow_view}>
                    <Text
                      style={[
                        Styles.bold_body_label,
                        {marginHorizontal: 15, fontSize: 10},
                      ]}>
                      {Locale('Goalprize: Campaigns I participated and lost')}
                    </Text>
                  </View>
                </View>
                <FlatList
                  style={[{marginTop: 20}]}
                  data={this.props.lostCampaignData}
                  renderItem={({item, index}) => (
                    <TouchableWithoutFeedback
                      onPress={() =>
                        this.props.navigation.navigate('CampaignDetail', {
                          tabScreen: true,
                          data: item,
                          redrectionFromLogin: false,
                        })
                      }>
                      <View
                        style={[
                          Styles.shadow_view,
                          {
                            width: wp(45),
                            borderRadius: 5,
                            overflow: 'hidden',
                            backgroundColor: COLOR.DARK_BLUE,
                            marginLeft: 10,
                          },
                        ]}>
                        <View
                          style={{
                            margin: 0,
                            padding: 10,
                          }}>
                          <View
                            style={{
                              flexDirection: 'row',
                              justifyContent: 'space-between',
                              margin: 0,
                            }}>
                            <Text
                              style={[Styles.body_label, style.campaign_view]}>
                              {item.campaign
                                ? item.campaign.name
                                : item.campaignTemplate
                                ? item.campaignTemplate.name
                                : ''}
                            </Text>
                            <Image
                              source={IMAGES.mono}
                              style={{
                                height: 30,
                                width: 30,
                                marginLeft: 5,
                              }}
                              resizeMode={'contain'}
                            />
                          </View>
                          <View
                            style={{
                              flexDirection: 'row',
                              flex: 1,
                              marginTop: 10,
                            }}>
                            <View style={{flex: 1, marginRight: 10}}>
                              <Text
                                style={[
                                  Styles.extra_small_label,
                                  {margin: 0, marginTop: 5, color: COLOR.WHITE},
                                ]}>
                                {Locale('My total spent')}
                              </Text>
                              <Text
                                style={[
                                  Styles.small_label,
                                  {
                                    margin: 0,
                                    marginTop: 5,
                                    color: COLOR.WHITE,
                                    fontWeight: '600',
                                  },
                                ]}>
                                {insertComma(item.totalCredits)} THB
                              </Text>
                            </View>
                            <View style={{width: 60}}>
                              <Text
                                style={[
                                  Styles.extra_small_label,
                                  {margin: 0, marginTop: 5, color: COLOR.WHITE},
                                ]}>
                                {Locale('My rank')}
                              </Text>
                              <Text
                                style={[
                                  Styles.small_label,
                                  {
                                    margin: 0,
                                    marginTop: 3,
                                    color: COLOR.WHITE,
                                    fontWeight: '600',
                                  },
                                ]}>
                                {item.rank}
                              </Text>
                            </View>
                          </View>
                        </View>
                      </View>
                    </TouchableWithoutFeedback>
                  )}
                  horizontal={true}
                  ListHeaderComponent={
                    this.props.lostCampaignData
                      ? this.props.lostCampaignData.length > 0
                        ? null
                        : this.noCampaignMessage('You campaign(s) found.')
                      : this.noCampaignMessage('You campaign(s) found.')
                  }
                  showsVerticalScrollIndicator={false}
                  showsHorizontalScrollIndicator={false}
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>
              <View
                style={[
                  style.shadowView,
                  Styles.shadow_view,
                  {marginVertical: 15, padding: 15},
                ]}>
                <this.campaignView
                  id={2}
                  showLine={false}
                  image={IMAGES.badge}
                  title={Locale('Settings')}
                />
              </View>
              <View
                style={{
                  width: wp(110),
                  alignSelf: 'center',
                  paddingVertical: 0,
                }}>
                <CustomButton
                  onPress={() => this.handleRedeemCredit()}
                  text={Locale('+ Request points')}
                />
              </View>
            </ScrollView>
          </SafeAreaView>
        </LinearGradient>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadowView: {
    backgroundColor: COLOR.LIGHT_BLUE,
    borderRadius: 5,
    width: wp(90),
    alignSelf: 'center',
    marginVertical: 20,
    paddingVertical: 15,
  },
  date_yellow_view: {
    height: 25,
    borderTopRightRadius: 15,
    borderBottomRightRadius: 15,
    marginLeft: 0,
    backgroundColor: COLOR.WHITE,
    justifyContent: 'center',
    alignSelf: 'flex-start',
  },
  campaign_view: {
    color: COLOR.WHITE,
    margin: 0,
    alignSelf: 'flex-start',
    flex: 1,
    fontWeight: '700',
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    userDetail: state.authReducer.userInfo,
    wonGoalprizeData: state.campaignReducer.myCreditDate,
    participatedCampaignData: state.campaignReducer.participatedCampaignData,
    lostCampaignData: state.campaignReducer.lostCampaignData,
    notificationData: state.authReducer.userNotification,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getUserDetail: val => {
      dispatch(AuthenticationAction.getUserDetail(val));
    },
    getMyWonGoalprize: val => {
      dispatch(CampaignActions.getMyWonGoalprize(val));
    },
    getLostCampaignData: val => {
      dispatch(CampaignActions.getLostCampaignData(val));
    },
    getMyParticipatedCampaign: val => {
      dispatch(CampaignActions.getMyParticipatedCampaign(val));
    },
    getUserNotification: val => {
      dispatch(AuthenticationAction.getNotifications(val));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DashboardScreen);
