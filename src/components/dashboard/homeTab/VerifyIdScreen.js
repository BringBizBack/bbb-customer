import React from 'react';
import {View, Text, SafeAreaView, StyleSheet} from 'react-native';
import Styles from '../../../styles/Styles';
import IMAGES from '../../../styles/Images';
import NavigationBar from '../../../commonView/NavigationBar';
import COLOR from '../../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {FloatingTitleTextInputField} from '../../../commonView/FloatingTitleTextInputField';
import {CustomButton} from '../../../commonView/CustomButton';
import LinearGradient from 'react-native-linear-gradient';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {showAlertMessage} from '../../../commonView/Helpers';
import AuthenticationAction from '../../../redux/action/AuthenticationAction';
import {connect} from 'react-redux';
import API from '../../../api/Api';
import Auth from '../../../auth';
import {AS_DEFAULT_HOME_SCREEN} from '../../../commonView/Constants';

let api = new API();
let auth = new Auth();

class VerifyIdScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      cardType: '',
      cardNumber: '',
      firstName: '',
      middleName: '',
      lastName: '',
      dob: '',
      birthPlace: '',
      gender: '',
      issueDate: '',
      expiryDate: '',
      nationality: '',
      phoneNumber: '',
    };

    this.updateMasterState = this.updateMasterState.bind(this);
    this.saveBusinessInfo = this.saveBusinessInfo.bind(this);
  }

  componentDidMount() {
    this.props.getUserDetail();
    this.props.getUserInfo(1);
  }

  updateMasterState = (props, val) => {
    this.setState({
      [props]: val,
    });
  };

  saveBusinessInfo = () => {
    if (this.state.idCard == '') {
      showAlertMessage('Enter card type');
    } else if (this.state.nationality == '') {
      showAlertMessage('Enter nationality');
    } else if (this.state.cardNumber == '') {
      showAlertMessage('Enter card number');
    } else if (this.state.firstName == '') {
      showAlertMessage('Enter first name');
    } else if (this.state.lastName == '') {
      showAlertMessage('Enter last name');
    } else if (this.state.contactphone == '') {
      showAlertMessage('Enter phone number');
    } else {
      let data = JSON.stringify({
        firstName: this.state.firstName,
        lastName: this.state.lastName,
        email: this.props.userDetail ? this.props.userDetail.email : '',
        phone: this.state.phoneNumber,
        cardType: this.state.cardType,
        cardNumber: this.state.cardNumber,
        idFront: '',
        idBack: '',
        profile: '',
      });
      let that = this;
      that.setState({isLoading: true});
      api
        .updateUserdetail(data)
        .then(json => {
          console.log('response is:-', json);
          that.setState({isLoading: false});
          if (json.status == 200) {
            this.props.getUserInfo(1);
            auth.setValue(AS_DEFAULT_HOME_SCREEN, 'DashboardScreen');
            showAlertMessage('Successfully saved response', 1);
            this.props.navigation.navigate('SuccessScreen');
          } else if (json.status == 400) {
            showAlertMessage(json.data.message, 2);
          } else {
            showAlertMessage(json.data.message, 2);
          }
        })
        .catch(error => {
          console.log('response is:-', error);
          that.setState({isLoading: false});
          showAlertMessage(error.response.data.message, 2);
        });

      this.setState({
        alertModalVisible: true,
      });
    }
  };

  render() {
    return (
      <View style={Styles.container}>
        <LinearGradient
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
          colors={[COLOR.TOP_GRADIENT, COLOR.BOTTOM_GRADIENT]}
          style={Styles.container}>
          <SafeAreaView style={Styles.container}>
            <NavigationBar />
            <KeyboardAwareScrollView>
              <View style={[style.shadowView]}>
                <Text
                  style={[
                    Styles.heading_label,
                    {
                      color: COLOR.WHITE,
                      alignSelf: 'flex-start',

                      fontSize: 20,
                    },
                  ]}>
                  Verify your ID:
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {
                      color: COLOR.WHITE,
                      alignSelf: 'flex-start',
                      marginVertical: 10,
                    },
                  ]}>
                  To protect your account from identity theft, fill in info from
                  a government-issued ID card that has your photo.
                </Text>
                <Text
                  style={[
                    Styles.button_font,
                    {
                      color: COLOR.YELLOW,
                      alignSelf: 'flex-start',
                    },
                  ]}>
                  To redeem credits this is required.
                </Text>
                <View
                  style={[
                    Styles.shadow_view,
                    style.shadowView,
                    {
                      backgroundColor: COLOR.LIGHT_BLUE,
                      paddingHorizontal: 15,
                      paddingVertical: 15,
                    },
                  ]}>
                  <FloatingTitleTextInputField
                    attrName={'cardType'}
                    image={IMAGES.down_arrow}
                    value={this.state.cardType}
                    updateMasterState={this.updateMasterState}
                    title={'Card Type'}
                  />
                  <FloatingTitleTextInputField
                    attrName={'nationality'}
                    value={this.state.nationality}
                    image={IMAGES.down_arrow}
                    updateMasterState={this.updateMasterState}
                    title={'Nationality'}
                  />
                  <FloatingTitleTextInputField
                    attrName={'cardNumber'}
                    value={this.state.cardNumber}
                    updateMasterState={this.updateMasterState}
                    title={'Card number'}
                  />
                  <FloatingTitleTextInputField
                    attrName={'firstName'}
                    value={this.state.firstName}
                    updateMasterState={this.updateMasterState}
                    title={'First name'}
                  />
                  <FloatingTitleTextInputField
                    attrName={'lastName'}
                    value={this.state.lastName}
                    updateMasterState={this.updateMasterState}
                    title={'Last Name'}
                  />
                  <FloatingTitleTextInputField
                    attrName={'phoneNumber'}
                    value={this.state.phoneNumber}
                    keyboardType={'number-pad'}
                    updateMasterState={this.updateMasterState}
                    title={'Phone Number'}
                  />
                </View>
                <View style={{marginVertical: 15}}>
                  <CustomButton
                    onPress={() => this.saveBusinessInfo()}
                    text={'Save'}
                  />
                </View>
              </View>
            </KeyboardAwareScrollView>
          </SafeAreaView>
        </LinearGradient>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadowView: {
    borderRadius: 10,
    width: wp(90),
    alignSelf: 'center',
    marginVertical: 20,
    paddingHorizontal: 0,
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    userDetail: state.authReducer.userDetail,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getUserDetail: val => {
      dispatch(AuthenticationAction.getUserInfo(val));
    },
    getUserInfo: val => {
      dispatch(AuthenticationAction.getUserDetail(val));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(VerifyIdScreen);
