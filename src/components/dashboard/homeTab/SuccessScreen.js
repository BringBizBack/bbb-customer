import React from 'react';
import {View, Text, SafeAreaView, StyleSheet, Image} from 'react-native';
import Styles from '../../../styles/Styles';
import IMAGES from '../../../styles/Images';
import COLOR from '../../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {CustomButton} from '../../../commonView/CustomButton';
import LinearGradient from 'react-native-linear-gradient';
import {Locale} from '../../../commonView/Helpers';

export default class SuccessScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      email: '',
      password: '',
      value: '',
      index: 0,
    };

    this.updateMasterState = this.updateMasterState.bind(this);
  }

  updateMasterState = props => {
    // this.setState({
    //     [props.name]:props.value
    // })
  };

  render() {
    return (
      <View style={Styles.container}>
        <LinearGradient
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
          colors={[COLOR.TOP_GRADIENT, COLOR.BOTTOM_GRADIENT]}
          style={Styles.container}>
          <SafeAreaView style={[Styles.container]}>
            <View style={{width: wp(90), alignSelf: 'center'}}>
              <Image
                style={{height: 35, marginLeft: 10, width: 120}}
                resizeMode={'contain'}
                source={IMAGES.logo}
              />
            </View>

            <View
              style={[
                style.shadowView,
                {
                  backgroundColor: 'transparent',
                  alignItems: 'center',
                  justifyContent: 'center',
                  flex: 1,
                },
              ]}>
              <Image
                source={IMAGES.security}
                style={{width: '100%', height: 200, marginTop: 0}}
                resizeMode={'contain'}
              />
              <Text
                style={[
                  Styles.heading_label,
                  {
                    color: COLOR.WHITE,
                    alignSelf: 'center',
                    fontSize: 16,
                    marginTop: 50,
                  },
                ]}>
                {Locale('Welcome to GoalPrize!')}
              </Text>
              <Text
                style={[
                  Styles.small_label,
                  {
                    color: COLOR.WHITE,
                    alignSelf: 'center',
                    marginTop: 5,
                    textAlign: 'center',
                  },
                ]}>
                {Locale('Your email has been verified')} {'\n'} $
                {Locale('Now you can participate in GoalPrizes')}
              </Text>

              <View
                style={{
                  width: wp(60),
                  alignSelf: 'center',
                  marginTop: 30,
                }}>
                <CustomButton
                  onPress={() =>
                    this.props.navigation.navigate('DashboardScreen')
                  }
                  text={Locale('Get Started')}
                />
              </View>
            </View>
          </SafeAreaView>
        </LinearGradient>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadowView: {
    backgroundColor: COLOR.LIGHT_BLUE,
    borderRadius: 10,
    width: wp(90),
    alignSelf: 'center',
    marginVertical: 20,
    paddingVertical: 10,
    paddingHorizontal: 15,
  },
  codeFieldRoot: {
    marginTop: 20,
    width: wp(70),
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  cellRoot: {
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: '#ccc',
    borderBottomWidth: 4,
  },
  cellText: {
    color: COLOR.BLACK,
    fontSize: 26,
    textAlign: 'center',
  },
  focusCell: {
    borderBottomColor: COLOR.GREEN,
    borderBottomWidth: 4,
  },
});
