import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  TextInput,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Switch,
  Platform,
} from 'react-native';
import Styles from '../../../styles/Styles';
import IMAGES from '../../../styles/Images';
import NavigationBar from '../../../commonView/NavigationBar';
import COLOR from '../../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {CustomButton} from '../../../commonView/CustomButton';
import LinearGradient from 'react-native-linear-gradient';

import AlertPopup from '../../../commonView/AlertPopup';
import AuthenticationAction from '../../../redux/action/AuthenticationAction';
import {connect} from 'react-redux';
import {
  handlingGPNumber,
  Locale,
  navigate,
  showAlertMessage,
} from '../../../commonView/Helpers';
import ImagePicker from 'react-native-image-crop-picker';
import ActionSheet from '../../../commonView/ActionSheet';
import {
  AS_DEFAULT_HOME_SCREEN,
  AS_FCM_TOKEN,
  AS_INITIAL_ROUTE,
  AS_USER_DETAIL,
  AS_USER_TOKEN,
  U_BASE,
  U_FILE_UPLOAD,
} from '../../../commonView/Constants';
import ActivityIndicator from '../../../commonView/ActivityIndicator';
import Auth from '../../../auth';
import {CommonActions} from '@react-navigation/native';
import API from '../../../api/Api';
import {Dropdown} from '../../../commonView/Dropdown';

let auth = new Auth();
let api = new API();

class SettingsScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      email: '',
      name: '',
      id: '',
      idCard: null,
      phoneNumber: '',
      idNumber: '',
      profileImage: null,
      showVerifiedModal: false,
      isEditable: false,
      actionModalVisible: false,
      emailPrivate: false,
      isLoading: false,
      cardTypeData: [
        {id: 1, label: 'Thai ID', value: 'Thai ID'},
        {id: 2, label: 'Passport', value: 'Passport'},
      ],
    };

    this.updateMasterState = this.updateMasterState.bind(this);
    this.closeVerifyModal = this.closeVerifyModal.bind(this);
    this.updateUserDetail = this.updateUserDetail.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
    this.initializeView = this.initializeView.bind(this);
    this.handleSheet = this.handleSheet.bind(this);
    this.chooseImage = this.chooseImage.bind(this);
    this.handleDropdown = this.handleDropdown.bind(this);
  }

  componentDidMount() {
    this.props.getUserDetail();
    if (this.props.userDetail) {
      this.initializeView(this.props.userDetail);
    }
  }

  componentDidUpdate(
    prevProps: Readonly<P>,
    prevState: Readonly<S>,
    snapshot: SS,
  ) {
    if (prevProps.userDetail != this.props.userDetail) {
      this.initializeView(this.props.userDetail);
    }
  }

  initializeView(val) {
    console.log('user detail:-', val);
    if (val) {
      this.setState({
        email: val.email ?? '',
        firstName: val.firstName,
        lastName: val.lastName,
        id: val.bbb_id ?? val.id,
        idCard: val.cardType == '' ? null : val.cardType,
        phoneNumber: val.phone,
        idNumber: val.cardNumber,
        profileImage: val.profile,
        isEditable: val.firstName == '' ? true : false,
        emailPrivate: val.emailPrivate,
      });
    }
  }

  updateMasterState(name, value) {
    if (name == 'phoneNumber') {
      if (value.length <= 10) {
        this.setState({
          [name]: value,
        });
      }
    } else {
      this.setState({
        [name]: value,
      });
    }
  }

  handleDropdown(val) {
    if (val) {
      let card = {id: val == 'Passport' ? 2 : 1, label: val, value: val};
      if (card) {
        this.setState({
          idCard: card,
          isLoading: false,
        });
      }
    }
  }

  updateUserDetail() {
    console.log(this.state.name.split(' '));
    if (this.state.isEditable) {
      // if (this.state.idCard == '') {
      //   showAlertMessage(Locale('Enter card type'));
      // } else if (this.state.nationality == '') {
      //   showAlertMessage(Locale('Enter nationality'));
      // } else if (this.state.cardNumber == '') {
      //   showAlertMessage(Locale('Enter card number'));
      // } else
      // if (this.state.firstName == '') {
      //   showAlertMessage(Locale('Enter first name'));
      // } else if (this.state.lastName == '') {
      //   showAlertMessage(Locale('Enter last name'));
      // } else if (this.state.contactphone == '') {
      //   showAlertMessage(Locale('Enter phone number'));
      // } else {
      let split = this.state.name.split(' ');

      let data = JSON.stringify({
        firstName: this.state.firstName,
        lastName: this.state.lastName,
        email: this.props.userDetail ? this.props.userDetail.email : '',
        phone: this.state.phoneNumber,
        cardType: this.state.idCard,
        cardNumber: this.state.idNumber,
        emailPrivate: this.state.emailPrivate,
        idFront: '',
        idBack: '',
        profile: this.state.profileImage,
      });
      let that = this;
      that.setState({isLoading: true});
      api
        .updateUserdetail(data)
        .then(json => {
          console.log('response is:-', json);
          that.setState({isLoading: false});
          if (json.data.status == 200) {
            this.props.getUserDetail(1);
            auth.setValue(AS_DEFAULT_HOME_SCREEN, 'DashboardScreen');
            showAlertMessage(Locale('Successfully saved response'), 1);
            this.props.navigation.goBack();
          } else if (json.data.status == 400) {
            showAlertMessage(json.data.message, 2);
          } else {
            showAlertMessage(json.data.message, 2);
          }
        })
        .catch(error => {
          console.log('response is:-', error);
          that.setState({isLoading: false});
          showAlertMessage(error.response.data.message, 2);
        });

      this.setState({
        alertModalVisible: true,
      });
      //}
    } else {
      this.setState({isEditable: !this.state.isEditable});
    }
  }

  commonView = props => {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          alignItems: 'center',
          marginBottom: 15,
        }}>
        <Text style={[Styles.body_label, style.titleLabel]}>{props.title}</Text>
        <TextInput
          editable={
            this.state.isEditable
              ? props.editable
                ? props.editable
                : false
              : false
          }
          returnKeyLabel={'Done'}
          returnKeyType={'done'}
          multiline={false}
          keyboardType={props.keyboardType ? props.keyboardType : 'default'}
          onChangeText={val => this.updateMasterState(props.name, val)}
          style={[
            Styles.small_label,
            {
              color: COLOR.WHITE,
              flex: 1,
              margin: 0,
              fontSize: 13,
              borderBottomWidth: this.state.isEditable ? 1 : 0,
              borderBottomColor: COLOR.WHITE,
            },
          ]}
          value={props.value}
        />
      </View>
    );
  };

  closeVerifyModal() {
    this.setState({showVerifiedModal: false});
  }

  handleSheet(val) {
    if (val == 1) {
      this.chooseImage(1);
    } else if (val == 2) {
      this.chooseImage(2);
    } else if (val == 3) {
      this.setState({
        actionModalVisible: false,
      });
    }
  }

  chooseImage(val) {
    let options = {
      title: Locale('Select Image'),
      compressImageMaxWidth: 2200,
      compressImageMaxHeight: 1000,
      multiple: false,
      cropping: true,
      freeStyleCropEnabled: true,
    };

    if (val == 1) {
      ImagePicker.openCamera({
        title: Locale('Select Image'),
        compressImageMaxWidth: 2200,
        compressImageMaxHeight: 1000,
        cropping: true,
        freeStyleCropEnabled: true,
      })
        .then(response => {
          if (response.didCancel) {
            console.log('User cancelled  Camera');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
            showAlertMessage(response.customButton);
          } else {
            this.setState({
              isLoading: false,
              actionModalVisible: false,
            });
            this.callUploadImage(response, val);
          }
        })
        .catch(error => {
          if (error.message) {
            showAlertMessage(error.message);
          }
        });
    } else if (val == 2) {
      ImagePicker.openPicker(options)
        .then(response => {
          console.log('image local reposnse', response);
          if (response.didCancel) {
            console.log('User cancelled image picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
            alert(response.customButton);
          } else {
            this.setState({
              isLoading: false,
              actionModalVisible: false,
            });

            this.callUploadImage(response, val);
          }
        })
        .catch(error => {
          if (error.message) {
            showAlertMessage(error.message);
          }
        });
    }
  }

  callUploadImage(res, val) {
    var data = new FormData();
    var photo = {
      uri: res.path,
      type: res.type ? res.type : res.mime,
      name: 'image.jpg',
    };
    console.log('image uploading data is:-', photo);
    //this.setState({isLoading: true});
    data.append('file', photo);
    fetch(U_BASE + U_FILE_UPLOAD, {
      body: data,
      method: 'POST',
      headers: {
        Accept: 'application/json',
        Authorization: 'Bearer ' + this.state.token,
      },
    })
      .then(response => response.json())
      .catch(error => {
        console.log('error data is:-', error);
        this.setState({isLoading: false});
      })
      .then(responseData => {
        this.setState({isLoading: false});
        console.log('response data is:-', responseData);
        if (responseData !== undefined) {
          this.setState(
            {
              profileImage: responseData.response.imageUrl,
              isLoading: false,
            },
            () => {
              showAlertMessage(Locale('Image uploaded successfully'), 1);
            },
          );
        } else {
          this.setState({isLoading: false});
          showAlertMessage(Locale('Image uploading failed'));
        }
      })
      .done();
  }

  async handleLogout() {
    const token = await auth.getValue(AS_FCM_TOKEN);
    api
      .logoutUser(token)
      .then(json => {
        console.log('login response is:-', json);
        this.setState({isLoading: true});
        if (json.status == 200) {
          this.setState({isLoading: false});
          auth.remove(AS_USER_TOKEN);
          auth.remove(AS_USER_DETAIL);
          auth.remove(AS_DEFAULT_HOME_SCREEN);
          auth.setValue(AS_INITIAL_ROUTE, 'LoginScreen');
          showAlertMessage(Locale('Successfully logged out user'));
          this.props.navigation.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [{name: 'LoginScreen'}],
            }),
          );
          this.props.logoutUser();
        } else if (json.status == 400) {
          showAlertMessage(json.data.message, 2);
        } else {
          showAlertMessage(json.data.message, 2);
        }
      })
      .catch(error => {
        this.setState({isLoading: false});
        showAlertMessage(error.response.data.message, 2);
      });
  }

  render() {
    console.log(this.state.idCard);
    return (
      <View style={Styles.container}>
        <LinearGradient
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
          colors={[COLOR.TOP_GRADIENT, COLOR.BOTTOM_GRADIENT]}
          style={Styles.container}>
          <AlertPopup
            prop={this.props}
            closeVerifyModal={this.closeVerifyModal}
            modalVisible={this.state.showVerifiedModal}
          />
          <ActivityIndicator loading={this.state.isLoading} />
          <SafeAreaView style={Styles.container}>
            <NavigationBar title={'Settings'} />

            <ScrollView>
              <View style={[style.shadowView, Styles.shadow_view]}>
                <this.commonView
                  title={Locale('First Name')}
                  editable={true}
                  multiline={false}
                  name={'firstName'}
                  value={this.state.firstName}
                />
                <this.commonView
                  title={Locale('Last Name')}
                  editable={true}
                  multiline={false}
                  name={'lastName'}
                  value={this.state.lastName}
                />
                <this.commonView
                  title={Locale('Goal Prize ID')}
                  editable={false}
                  value={handlingGPNumber(this.state.id)}
                />
                <this.commonView
                  editable={false}
                  title={Locale('Email')}
                  value={this.state.email}
                />
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginBottom: 15,
                  }}>
                  <Text style={[Styles.body_label, style.titleLabel]}>
                    Leaderboard
                  </Text>
                  <Text
                    style={[
                      Styles.extra_small_label,
                      style.titleLabel,
                      {width: null, textAlign: 'center'},
                    ]}>
                    First 5{'\n'}of Email
                  </Text>
                  <Switch
                    disabled={!this.state.isEditable}
                    trackColor={COLOR.LIGHT_TEXT}
                    ios_backgroundColor={COLOR.LIGHT_TEXT}
                    tintColor={COLOR.GREEN}
                    style={{marginHorizontal: 10}}
                    onValueChange={val => {
                      console.log(val);
                      this.setState({emailPrivate: !this.state.emailPrivate});
                    }}
                    value={this.state.emailPrivate}
                  />
                  <Text
                    style={[
                      Styles.extra_small_label,
                      style.titleLabel,
                      {width: null},
                    ]}>
                    Private
                  </Text>
                </View>
                <this.commonView
                  title={Locale('Phone number')}
                  editable={true}
                  keyboardType={'number-pad'}
                  name={'phoneNumber'}
                  value={this.state.phoneNumber}
                />
                <Text
                  style={[
                    Styles.extra_small_label,
                    style.titleLabel,
                    {width: null, marginVertical: 10},
                  ]}>
                  The first time you redeem credits the ID you are holding must
                  match the ID you place here (first name, last name, ID type,
                  ID number). We do this to protect your account from identity
                  theft.
                </Text>
                <View
                  style={{
                    marginBottom: 10,
                    alignItems: 'center',
                    flexDirection: 'row',
                    ...(Platform.OS == 'ios' && {zIndex: 1000}),
                  }}>
                  <Text style={[Styles.bold_body_label, style.titleLabel]}>
                    {Locale('ID Card Type')}
                  </Text>
                  <View style={{flex: 1}}>
                    <View
                      style={[
                        Styles.shadow_view,
                        style.shadowView,
                        {
                          flex: 1,
                          zIndex: 1000,
                          marginRight: 15,
                          paddingVertical: 0,
                        },
                      ]}
                    />
                    <View style={{flex: 1, marginTop: -30}}>
                      <Dropdown
                        placeholder={
                          this.state.idCard
                            ? this.state.idCard.value
                              ? this.state.idCard.value
                              : Locale('ID Card Type')
                            : Locale('ID Card Type')
                        }
                        placeholdeTextColor={COLOR.WHITE}
                        type={10}
                        zIndex={3002}
                        value={
                          this.state.idCard
                            ? this.state.idCard.value
                              ? this.state.idCard.value
                              : ''
                            : ''
                        }
                        items={this.state.cardTypeData}
                        handleDropdown={this.handleDropdown}
                      />
                    </View>
                  </View>
                </View>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <this.commonView
                    editable={true}
                    title={Locale('ID card number')}
                    keyboardType={'number-pad'}
                    name={'idNumber'}
                    value={this.state.idNumber}
                  />
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({showVerifiedModal: true});
                    }}>
                    <Text
                      style={[
                        Styles.extra_small_label,
                        {
                          width: 80,
                          color: COLOR.WHITE,
                          marginBottom: 10,
                          textDecorationLine: 'underline',
                        },
                      ]}>
                      {Locale('Verified')}
                    </Text>
                  </TouchableOpacity>
                </View>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                  }}>
                  {/*<Text style={[Styles.bold_body_label, style.titleLabel]}>*/}
                  {/*  Profile Image*/}
                  {/*</Text>*/}
                  <TouchableWithoutFeedback
                    onPress={() =>
                      this.state.isEditable &&
                      this.setState({actionModalVisible: true})
                    }>
                    <View
                      style={[
                        Styles.shadow_view,
                        {
                          backgroundColor: COLOR.MEDIUM_BLUE,
                          height: 150,
                          width: wp(80),
                          alignItems: 'center',
                          justifyContent: 'center',
                          alignSelf: 'center',
                          marginTop: 50,
                        },
                      ]}>
                      <Image
                        source={
                          this.state.profileImage
                            ? {uri: this.state.profileImage}
                            : IMAGES.plus_circle
                        }
                        style={
                          this.state.profileImage
                            ? {height: '100%', width: '100%'}
                            : {height: 35, width: 35}
                        }
                      />
                      {!this.state.profileImage && (
                        <Text
                          style={[
                            Styles.extra_small_label,
                            {
                              color: COLOR.WHITE,
                            },
                          ]}>
                          {Locale('Upload profile image')}
                        </Text>
                      )}
                    </View>
                  </TouchableWithoutFeedback>
                </View>
              </View>
              <View style={style.bottomButton}>
                <CustomButton
                  onPress={this.updateUserDetail}
                  text={this.state.isEditable ? 'Save' : 'Edit'}
                />
                <CustomButton
                  bg={'transparent'}
                  labelColor={COLOR.WHITE}
                  onPress={this.handleLogout}
                  text={Locale('Logout')}
                />
              </View>
              <TouchableWithoutFeedback
                onPress={() => {
                  navigate('PrivacyPolicyScreen');
                }}>
                <Text
                  style={{
                    textDecorationLine: 'underline',
                    alignSelf: 'center',
                    marginTop: 20,
                    marginBottom: 10,
                    color: COLOR.WHITE,
                  }}>
                  {Locale('Privacy Policy')}
                </Text>
              </TouchableWithoutFeedback>
              <Text
                style={[
                  Styles.small_label,
                  {color: COLOR.WHITE, alignSelf: 'center', marginBottom: 10},
                ]}>
                {Platform.OS == 'ios' ? 'version-1.0.18' : 'version-1.0.9'}
              </Text>
            </ScrollView>
            <ActionSheet
              modalVisible={this.state.actionModalVisible}
              handleSheet={this.handleSheet}
            />
          </SafeAreaView>
        </LinearGradient>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadowView: {
    backgroundColor: COLOR.LIGHT_BLUE,
    borderRadius: 10,
    width: wp(90),
    alignSelf: 'center',
    marginVertical: 15,
    paddingVertical: 30,
    paddingHorizontal: 15,
  },
  bottomButton: {marginVertical: 20, width: wp(90), alignSelf: 'center'},
  titleLabel: {color: COLOR.WHITE, width: 110, margin: 0, fontSize: 13},
});

const mapStateToProps = (state, ownProps) => {
  return {
    userDetail: state.authReducer.userInfo,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getUserDetail: val => {
      dispatch(AuthenticationAction.getUserDetail(val));
    },
    logoutUser: val => {
      dispatch(AuthenticationAction.logoutUser());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SettingsScreen);
