import React from 'react';
import {
  View,
  SafeAreaView,
  StyleSheet,
  Image,
  TouchableWithoutFeedback,
  Text,
  FlatList,
  RefreshControl,
  Alert,
} from 'react-native';
import Styles from '../../styles/Styles';
import IMAGES from '../../styles/Images';
import COLOR from '../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {FloatingTitleTextInputField} from '../../commonView/FloatingTitleTextInputField';
import {CustomButton} from '../../commonView/CustomButton';
import CalendarPicker from 'react-native-calendar-picker';
import LinearGradient from 'react-native-linear-gradient';
import NavigationBar from '../../commonView/NavigationBar';
import AuthenticationAction from '../../redux/action/AuthenticationAction';
import CampaignActions from '../../redux/action/CampaignActions';
import {connect} from 'react-redux';
import {
  getDateFromTimeStamp,
  insertComma,
  Locale,
  showAlertMessage,
} from '../../commonView/Helpers';
import ImagePicker from 'react-native-image-crop-picker';
import {U_BASE, U_FILE_UPLOAD} from '../../commonView/Constants';
import ActionSheet from '../../commonView/ActionSheet';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {Dropdown} from '../../commonView/Dropdown';
import Lightbox from 'react-native-lightbox';

class RedeemCreditScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      campaignData: null,
      selectedStartDate: null,
      showCalendar: false,
      isRedeemScreen: false,
      selectedCampaign: null,
      amountRequested: '',
      memberCount: 1,
      billNumber: 0,
      billImage: null,
      actionModalVisible: false,
      alertModalVisible: false,
      redeemData: null,
      amountRequestedValidation: false,
      billNumberValidation: false,
      campaignDropdownData: [],
      refreshing: false,
    };

    this.onDateChange = this.onDateChange.bind(this);
    this.updateMasterState = this.updateMasterState.bind(this);
    this.handleSubmitButton = this.handleSubmitButton.bind(this);
    this.handleDropdown = this.handleDropdown.bind(this);
    this.campaignDropdownData = this.campaignDropdownData.bind(this);
    this.userDetailView = this.userDetailView.bind(this);
  }

  componentDidMount() {
    if (this.props.route.params) {
      if (this.props.route.params.type) {
        this.setState({
          isRedeemScreen: this.props.route.params.type,
          redeemData: this.props.route.params.data,
        });
        if (this.props.route.params.type) {
          this.props.getCreditRedeemHistoryData(
            1,
            this.props.route.params.data
              ? this.props.route.params.data.id
              : null,
          );
        }
      }
    }
    this.props.getCampaignData();
    if (this.props.route.params && !this.props.route.params.type) {
      this.props.getCreditRedeemHistoryData(1);
    }
    // this.props.getCreditRequestHistoryData(1);
  }

  componentDidUpdate(
    prevProps: Readonly<P>,
    prevState: Readonly<S>,
    snapshot: SS,
  ) {
    if (prevProps.campaignData != this.props.campaignData) {
      this.setState(
        {
          campaignData: this.props.campaignData,
        },
        () => {
          this.campaignDropdownData();
        },
      );
    }
  }

  componentWillUnmount() {
    this.setState({
      selectedStartDate: null,
      showCalendar: false,
      isRedeemScreen: false,
      selectedCampaign: null,
      amountRequested: '',
      memberCount: 1,
      billNumber: 0,
      billImage: null,
      actionModalVisible: false,
      alertModalVisible: false,
      redeemData: null,
      amountRequestedValidation: false,
      billNumberValidation: false,
      campaignDropdownData: [],
    });
  }

  updateMasterState(attr, val) {
    if (attr == 'billNumber') {
      this.setState({
        [attr]: val.replace(',', ''),
      });
    } else {
      this.setState({
        [attr]: val,
        amountRequestedValidation: false,
        billNumberValidation: false,
      });
    }
  }

  handleDropdown(props) {
    let that = this;
    if (that.state.campaignData && that.state.campaignData.length > 0) {
      let campaign = that.state.campaignData.filter(item => {
        let name = item.campaignTemplate.name + ' (' + item.trading.name + ')';
        if (name == props) {
          return item;
        }
      })[0];
      if (campaign) {
        this.setState({
          selectedCampaign: campaign,
        });
      }
    }
  }

  commonView = props => {
    return (
      <View
        style={{
          flexDirection: 'row',
          width: '90%',
          alignSelf: 'center',
          marginTop: 10,
        }}>
        <Text
          style={[
            Styles.small_label,
            {width: 120, alignSelf: 'flex-start', color: COLOR.WHITE},
          ]}>
          {props.attr}
        </Text>
        <Text
          style={[
            Styles.small_label,
            {flex: 1, alignSelf: 'flex-start', color: COLOR.WHITE},
          ]}>
          {props.value}
        </Text>
      </View>
    );
  };

  campaignDropdownData() {
    if (this.state.campaignData && this.state.campaignData.length > 0) {
      let data = this.state.campaignData.map((val, index) => {
        let currentTime = parseInt(new Date().getTime() / 1000);
        if (this.state.isRedeemScreen) {
          return {
            id: val.id,
            label: val.campaignTemplate.name + ' (' + val.trading.name + ')',
            value: val.campaignTemplate.name + ' (' + val.trading.name + ')',
          };
        } else if (val.startTime > currentTime) {
          return null;
        } else {
          return {
            id: val.id,
            label: val.campaignTemplate.name + ' (' + val.trading.name + ')',
            value: val.campaignTemplate.name + ' (' + val.trading.name + ')',
          };
        }
      });
      let sortData = data.filter(item => item != null);
      this.setState({
        campaignDropdownData: sortData,
      });
    }
  }

  calendarView = props => {
    return (
      <View style={[Styles.container]}>
        <CalendarPicker onDateChange={this.onDateChange} />
      </View>
    );
  };

  onDateChange(date) {
    this.setState({
      selectedStartDate: date,
      showCalendar: false,
    });
  }

  getCampaignForRedeem() {
    if (this.state.isRedeemScreen) {
      return this.state.redeemData;
    } else {
      if (this.state.campaignData && this.state.campaignData.length > 0) {
        return this.state.selectedCampaign;
      } else {
        return null;
      }
    }
  }

  handleSubmitButton = type => {
    let campaign = this.getCampaignForRedeem();
    if (
      this.props.userDetail &&
      !this.props.userDetail.cardNumber &&
      type == 2
    ) {
      showAlertMessage(
        Locale(
          'Please submit profile details from settings before requesting for redeem',
        ),
      );
    } else if (campaign == null) {
      showAlertMessage(Locale('Select campaign'));
    } else {
      let visitorEligible = parseInt(
        parseInt(this.state.amountRequested) / campaign.minSpent,
      );
      if (this.state.amountRequested == '') {
        this.setState({amountRequestedValidation: true});
      } else if (this.state.amountRequested < campaign.minSpent && type != 2) {
        showAlertMessage(
          `Entered amount should be more than minimum spent.(${campaign.minSpent} THB)`,
        );
      } else if (
        this.state.isRedeemScreen &&
        this.state.redeemData &&
        this.state.amountRequested > this.state.redeemData.totalCredits
      ) {
        showAlertMessage(
          'Amount requested should be less than available balance.',
        );
      } else if (isNaN(visitorEligible)) {
        showAlertMessage('Enter correct amount');
      } else if (
        visitorEligible < this.state.memberCount &&
        this.state.memberCount > 1 &&
        type != 2
      ) {
        Alert.alert(
          'Alert',
          `Only ${visitorEligible} visits will be counted for this transaction as per criteria for minimum spent per visit under this campaign.`,
          [
            {
              text: 'Cancel',
              onPress: () => console.log('Cancel Pressed'),
              style: 'cancel',
            },
            {
              text: 'OK',
              onPress: () => {
                this.setState({
                  memberCount: visitorEligible,
                });
              },
            },
          ],
        );
      } else if (this.state.billNumber == '') {
        this.setState({billNumberValidation: true});
      } else if (this.state.billImage == null) {
        showAlertMessage(Locale('Upload Bill Image'));
      } else {
        let data = {
          trading: this.state.isRedeemScreen
            ? this.state.redeemData
              ? this.state.redeemData.trading.id
              : ''
            : campaign
            ? campaign.trading
              ? campaign.trading.id
              : 0
            : 0,
          billNumber: this.state.billNumber,
          campaign: this.state.isRedeemScreen
            ? this.state.redeemData
              ? this.state.redeemData.campaign.id
              : ''
            : campaign
            ? campaign.id
            : 0,
          amount: this.state.amountRequested,
          visitors: this.state.memberCount,
          bill: this.state.billImage,
          businessName: this.state.isRedeemScreen
            ? this.state.redeemData
              ? this.state.redeemData.trading.name
              : ''
            : campaign
            ? campaign.trading.name
            : '',
        };
        this.props.navigation.navigate('ScanScreen', {
          data: data,
          campaign: campaign,
          type: type,
        });

        this.setState({
          amountRequested: null,
          memberCount: 1,
          billNumber: 0,
          billImage: null,
        });
      }
    }
  };

  handleSheet = val => {
    if (val == 1) {
      this.chooseImage(1);
    } else if (val == 2) {
      this.chooseImage(2);
    } else if (val == 3) {
      this.setState({
        actionModalVisible: false,
      });
    }
  };

  chooseImage = val => {
    let options = {
      title: Locale('Select Image'),
      compressImageMaxWidth: 2200,
      compressImageMaxHeight: 1000,
      multiple: false,
      cropping: true,
    };

    if (val == 1) {
      ImagePicker.openCamera({
        title: Locale('Select Image'),
        compressImageMaxWidth: 2200,
        compressImageMaxHeight: 1000,
      })
        .then(response => {
          if (response.didCancel) {
            console.log('User cancelled  Camera');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
            showAlertMessage(response.customButton);
          } else {
            this.setState({
              isLoading: true,
              actionModalVisible: false,
            });
            this.callUploadImage(response, val);
          }
        })
        .catch(error => {
          if (error.message) {
            showAlertMessage(error.message);
          }
        });
    } else if (val == 2) {
      ImagePicker.openPicker(options)
        .then(response => {
          console.log('image local reposnse', response);
          if (response.didCancel) {
            console.log('User cancelled image picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
            alert(response.customButton);
          } else {
            this.setState({
              //isLoading: true,
              actionModalVisible: false,
            });

            this.callUploadImage(response, val);
          }
        })
        .catch(error => {
          if (error.message) {
            showAlertMessage(error.message);
          }
        });
    }
  };

  callUploadImage(res, val) {
    var data = new FormData();
    var photo = {
      uri: res.path,
      type: res.type ? res.type : res.mime,
      name: 'image.jpg',
    };
    console.log('image uploading data is:-', photo);
    data.append('file', photo);
    fetch(U_BASE + U_FILE_UPLOAD, {
      body: data,
      method: 'POST',
      headers: {
        Accept: 'application/json',
        Authorization: 'Bearer ' + this.state.token,
      },
    })
      .then(response => response.json())
      .catch(error => {
        console.log('error data is:-', error);
        this.setState({isLoading: false});
      })
      .then(responseData => {
        this.setState({isLoading: false});
        console.log('response data is:-', responseData);
        if (responseData.status == 200) {
          showAlertMessage(Locale('Image uploaded successfully'), 1);
          this.setState({billImage: responseData.response.imageUrl});
        } else {
          this.setState({isLoading: false});
          if (response.data && responseData.data.message) {
            showAlertMessage(responseData.data.message);
          }else if(response.message){
            showAlertMessage(responseData.message);
          }
        }
      })
      .done();
  }

  userDetailView() {
    return (
      <View
        style={[
          Styles.shadow_view,
          style.shadowView,
          {
            alignItems: 'center',
            justifyContent: 'center',
            marginTop: 20,
            width: wp(90),
            alignSelf: 'center',
          },
        ]}>
        <Text
          style={[
            Styles.small_label,
            {color: COLOR.WHITE, textAlign: 'center', marginHorizontal: 15},
          ]}>
          {Locale(
            'To request for points you first need to fill your profile first',
          )}
        </Text>
        <View style={{width: '50%', marginTop: 20}}>
          <CustomButton
            onPress={() => {
              this.props.navigation.navigate('SettingsScreen');
            }}
            text={Locale('Update profile')}
          />
        </View>
      </View>
    );
  }

  render() {
    let that = this;
    let {redeemData, amountRequestedValidation} = this.state;
    return (
      <View style={Styles.container}>
        {/*{this.state.showCalendar && <this.calendarView />}*/}
        <LinearGradient
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
          colors={[COLOR.TOP_GRADIENT, COLOR.BOTTOM_GRADIENT]}
          style={Styles.container}>
          <SafeAreaView style={Styles.container}>
            <NavigationBar
              title={
                this.state.isRedeemScreen
                  ? Locale('Redeem credits')
                  : Locale('Request points')
              }
            />
            <KeyboardAwareScrollView
              refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={() => {
                    this.setState({
                      refreshing: false,
                    });
                    this.props.getCreditRedeemHistoryData(
                      1,
                      this.state.isRedeemScreen
                        ? this.state.redeemData
                          ? this.state.redeemData.id
                          : null
                        : null,
                    );
                    //  this.props.getCreditRequestHistoryData(1);
                    this.campaignDropdownData();
                  }}
                />
              }>
              {this.props.userDetail &&
              this.props.userDetail.firstName != '' ? null : this.state
                  .isRedeemScreen ? (
                <this.userDetailView />
              ) : null}
              {this.state.isRedeemScreen && (
                <View
                  style={[
                    style.shadowView,
                    Styles.shadow_view,
                    {
                      padding: 20,
                      height: 180,
                      justifyContent: 'space-between',
                      backgroundColor: '#FC8686',
                      paddingVertical: 30,
                      marginBottom: 0,
                    },
                  ]}>
                  <View
                    style={{
                      width: '100%',
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                    }}>
                    <Image
                      source={
                        redeemData
                          ? {uri: redeemData.trading.logo}
                          : IMAGES.yakitori
                      }
                      style={{
                        height: 40,
                        width: 40,
                        borderRadius: 20,
                        marginRight: 10,
                      }}
                      resizeMode={'cover'}
                    />
                    <View style={{flex: 1}}>
                      <Text
                        style={[
                          Styles.button_font,
                          {
                            color: COLOR.WHITE,
                            alignSelf: 'flex-start',
                          },
                        ]}>
                        {redeemData ? redeemData.trading.name : ''}
                      </Text>
                      <Text
                        style={[
                          Styles.small_label,
                          {
                            color: COLOR.WHITE,
                            fontWeight: '600',
                          },
                        ]}>
                        {redeemData ? redeemData.campaignTemplate.name : ''}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      width: '100%',
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      alignSelf: 'center',
                      paddingHorizontal: 50,
                    }}>
                    <View style={{flex: 1}}>
                      <Text
                        style={[
                          Styles.extra_small_label,
                          {color: COLOR.WHITE, alignSelf: 'flex-start'},
                        ]}>
                        {Locale('Balance')}
                      </Text>
                      <Text
                        style={[
                          Styles.bold_body_label,
                          {
                            color: COLOR.WHITE,
                            alignSelf: 'flex-start',
                          },
                        ]}>
                        {redeemData ? insertComma(redeemData.totalCredits) : ''}{' '}
                        THB
                      </Text>
                    </View>
                    <View style={{flex: 1}}>
                      <Text
                        style={[
                          Styles.extra_small_label,
                          {color: COLOR.WHITE},
                        ]}>
                        {Locale('Expiry')}
                      </Text>
                      <Text
                        style={[
                          Styles.bold_body_label,
                          {
                            color: COLOR.WHITE,
                          },
                        ]}>
                        {redeemData
                          ? getDateFromTimeStamp(
                              redeemData.expiry,
                              'MMM DD, YYYY',
                            )
                          : ''}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      height: 35,
                      width: 85,
                      borderColor: COLOR.WHITE,
                      borderWidth: 1,
                      alignItems: 'center',
                      justifyContent: 'center',
                      borderRadius: 10,
                      alignSelf: 'flex-end',

                      top: 10,
                      right: 10,
                      position: 'absolute',
                    }}>
                    <TouchableWithoutFeedback
                      onPress={() =>
                        this.props.navigation.navigate('CampaignDetail', {
                          tabScreen: true,
                          data: this.state.redeemData,
                          redrectionFromLogin: false,
                        })
                      }>
                      <Text style={[Styles.small_label, {color: COLOR.WHITE}]}>
                        Details >>
                      </Text>
                    </TouchableWithoutFeedback>
                  </View>
                </View>
              )}

              <View
                style={[
                  Styles.shadow_view,
                  style.shadowView,
                  {
                    borderRadius: 5,
                    width: wp(90),
                    alignSelf: 'center',
                    marginVertical: 0,
                    paddingHorizontal: 15,
                    overflow: 'hidden',
                    marginTop: 15,
                  },
                ]}>
                <Text
                  style={[
                    Styles.button_font,
                    {
                      color: COLOR.WHITE,
                      alignSelf: 'flex-start',
                      marginVertical: 0,
                      fontWeight: '600',
                    },
                  ]}>
                  {this.state.isRedeemScreen
                    ? Locale('Redeem credits')
                    : Locale('Request points')}
                </Text>
                <Text
                  style={[
                    Styles.small_label,
                    {
                      color: COLOR.WHITE,
                      alignSelf: 'flex-start',
                      marginVertical: 5,
                    },
                  ]}>
                  {this.state.isRedeemScreen
                    ? Locale('Redeem credit here')
                    : Locale('After you pay the bill request points here')}
                </Text>
                <View
                  style={{
                    width: wp(90),
                    alignSelf: 'center',
                    paddingHorizontal: 15,
                  }}>
                  {!this.state.isRedeemScreen &&
                    this.state.campaignDropdownData.length > 0 && (
                      <Dropdown
                        placeholder={Locale('Select campaign')}
                        placeholdeTextColor={COLOR.WHITE}
                        zIndex={1000}
                        value={
                          this.state.selectedCampaign
                            ? this.state.selectedCampaign.campaignTemplate.name
                              ? this.state.selectedCampaign.campaignTemplate
                                  .name
                              : ''
                            : ''
                        }
                        items={this.state.campaignDropdownData}
                        handleDropdown={this.handleDropdown}
                      />
                    )}
                  {this.state.isRedeemScreen && (
                    <View>
                      <FloatingTitleTextInputField
                        isWhite={true}
                        editable={false}
                        value={
                          redeemData ? redeemData.campaignTemplate.name : ''
                        }
                        updateMasterState={this.updateMasterState}
                        title={Locale('Campaign Name')}
                      />
                    </View>
                  )}
                  <View style={{marginVertical: 15}}>
                    <FloatingTitleTextInputField
                      isWhite={true}
                      handleFieldEmpty={amountRequestedValidation}
                      keyboardType={'decimal-pad'}
                      attrName={'amountRequested'}
                      value={this.state.amountRequested}
                      updateMasterState={this.updateMasterState}
                      title={Locale('Amount Requested')}
                    />
                    <Text
                      style={[
                        Styles.extra_small_label,
                        {
                          color: COLOR.WHITE,
                          height: 15,
                          alignSelf: 'flex-start',
                          marginTop: -15,
                          textAlign: 'left',
                          fontSize: 10,
                        },
                      ]}>
                      {Locale('Do not include tax & tip')}
                    </Text>
                  </View>
                  <View style={{marginVertical: 15}}>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginBottom: 10,
                        paddingHorizontal: 10,
                      }}>
                      <TouchableWithoutFeedback
                        onPress={() => {
                          if (this.state.memberCount > 1) {
                            this.setState({
                              memberCount: this.state.memberCount - 1,
                            });
                          }
                        }}>
                        <Image
                          source={IMAGES.minus}
                          style={style.icon_image}
                          resizeMode={'contain'}
                        />
                      </TouchableWithoutFeedback>
                      <View
                        style={{
                          alignItems: 'center',
                          justifyContent: 'center',
                          flex: 1,
                        }}>
                        <Text
                          style={[
                            Styles.extra_small_label,
                            {color: COLOR.WHITE},
                          ]}>
                          {Locale('Visitor(s) in my group')}
                        </Text>
                        <View
                          style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                          }}>
                          <Image
                            source={IMAGES.user_yellow}
                            style={{height: 12, width: 12, marginRight: 5}}
                            resizeMode={'contain'}
                          />
                          <Text
                            style={[Styles.button_font, {color: COLOR.WHITE}]}>
                            {this.state.memberCount} {Locale('members')}
                          </Text>
                        </View>
                      </View>
                      <TouchableWithoutFeedback
                        onPress={() => {
                          this.setState({
                            memberCount: this.state.memberCount + 1,
                          });
                        }}>
                        <Image
                          source={IMAGES.plus}
                          style={style.icon_image}
                          resizeMode={'contain'}
                        />
                      </TouchableWithoutFeedback>
                    </View>
                    <View style={Styles.line_view} />
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 1, marginRight: 10}}>
                      <FloatingTitleTextInputField
                        isWhite={true}
                        handleFieldEmpty={this.state.billNumberValidation}
                        attrName={'billNumber'}
                        value={this.state.billNumber}
                        updateMasterState={this.updateMasterState}
                        title={Locale('Bill number')}
                      />
                    </View>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 1, marginRight: 10}}>
                      <CustomButton
                        bg={COLOR.WHITE}
                        onPress={() =>
                          this.setState({actionModalVisible: true})
                        }
                        image={IMAGES.camera}
                        text={Locale('Photograph Bill')}
                      />
                    </View>
                    <View style={{flex: 1}}>
                      <CustomButton
                        onPress={() =>
                          this.handleSubmitButton(
                            this.state.isRedeemScreen ? 2 : 1,
                          )
                        }
                        text={
                          this.state.isRedeemScreen
                            ? Locale('Redeem credits')
                            : Locale('Request points')
                        }
                      />
                    </View>
                  </View>
                  {this.state.billImage && (
                    <Lightbox
                      navigator={this.props.navigator}
                      underlayColor={'transparent'}
                      backgroundColor={'#ffffff'}
                      activeProps={{
                        style: {
                          width: 500,
                          height: 500,
                          alignSelf: 'center',
                        },
                        resizeMode: 'contain',
                      }}>
                      <Image
                        style={{
                          height: 100,
                          width: 100,
                          alignSelf: 'center',
                          marginTop: 15,
                        }}
                        resizeMode="cover"
                        source={{uri: this.state.billImage}}
                      />
                    </Lightbox>
                  )}
                </View>
              </View>
              {/*<View*/}
              {/*  style={[*/}
              {/*    Styles.shadow_view,*/}
              {/*    style.shadowView,*/}
              {/*    {marginBottom: 0, paddingBottom: 0},*/}
              {/*  ]}>*/}
              {/*  <View*/}
              {/*    style={{*/}
              {/*      width: '100%',*/}
              {/*      flexDirection: 'row',*/}
              {/*      justifyContent: 'space-between',*/}
              {/*      alignItems: 'center',*/}
              {/*    }}>*/}
              {/*    <View style={style.date_yellow_view}>*/}
              {/*      <Text*/}
              {/*        style={[*/}
              {/*          Styles.small_label,*/}
              {/*          {marginHorizontal: 15, color: COLOR.BLACK},*/}
              {/*        ]}>*/}
              {/*        Recent requests*/}
              {/*      </Text>*/}
              {/*    </View>*/}
              {/*    /!*<TouchableWithoutFeedback*!/*/}
              {/*    /!*  onPress={() => {*!/*/}
              {/*    /!*    this.setState({showCalendar: true});*!/*/}
              {/*    /!*  }}>*!/*/}
              {/*    /!*  <View*!/*/}
              {/*    /!*    style={{*!/*/}
              {/*    /!*      flexDirection: 'row',*!/*/}
              {/*    /!*      alignItems: 'center',*!/*/}
              {/*    /!*      borderBottomColor: COLOR.WHITE,*!/*/}
              {/*    /!*      borderBottomWidth: 1,*!/*/}
              {/*    /!*      marginRight: 10,*!/*/}
              {/*    /!*    }}>*!/*/}
              {/*    /!*    <Image*!/*/}
              {/*    /!*      source={IMAGES.calendar}*!/*/}
              {/*    /!*      style={{height: 15, width: 15, marginRight: 10}}*!/*/}
              {/*    /!*      resizeMode={'contain'}*!/*/}
              {/*    /!*    />*!/*/}
              {/*    /!*    <Text*!/*/}
              {/*    /!*      style={[*!/*/}
              {/*    /!*        Styles.extra_small_label,*!/*/}
              {/*    /!*        {color: COLOR.WHITE},*!/*/}
              {/*    /!*      ]}>*!/*/}
              {/*    /!*      Feb 01, 2021 - May 15, 2021*!/*/}
              {/*    /!*    </Text>*!/*/}
              {/*    /!*  </View>*!/*/}
              {/*    /!*</TouchableWithoutFeedback>*!/*/}
              {/*  </View>*/}

              {/*  <FlatList*/}
              {/*    style={style.flatlist_view}*/}
              {/*    data={this.props.creditRequestHistoryData}*/}
              {/*    renderItem={({item, index}) => (*/}
              {/*      <TouchableWithoutFeedback*/}
              {/*        onPress={() =>*/}
              {/*          this.props.navigation.navigate('CreditHistoryDetails', {*/}
              {/*            data: item,*/}
              {/*          })*/}
              {/*        }*/}
              {/*        style={{justifyContent: 'center', alignItems: 'center'}}>*/}
              {/*        <View*/}
              {/*          style={[*/}
              {/*            Styles.shadow_view,*/}
              {/*            {*/}
              {/*              width: '100%',*/}
              {/*              marginBottom: 5,*/}
              {/*            },*/}
              {/*          ]}>*/}
              {/*          <View*/}
              {/*            style={{*/}
              {/*              marginBottom: 10,*/}
              {/*              marginHorizontal: 0,*/}
              {/*              flexDirection: 'row',*/}
              {/*              alignItems: 'center',*/}
              {/*              justifyContent: 'space-between',*/}
              {/*            }}>*/}
              {/*            <View style={{margin: 0, flex: 1}}>*/}
              {/*              <Text*/}
              {/*                style={[*/}
              {/*                  Styles.button_font,*/}
              {/*                  {*/}
              {/*                    color: COLOR.WHITE,*/}
              {/*                    margin: 0,*/}
              {/*                    alignSelf: 'flex-start',*/}
              {/*                  },*/}
              {/*                ]}>*/}
              {/*                {insertComma(item ? item.amount : '')} THB*/}
              {/*              </Text>*/}
              {/*              <Text*/}
              {/*                style={[*/}
              {/*                  Styles.small_label,*/}
              {/*                  {*/}
              {/*                    color: COLOR.WHITE,*/}
              {/*                    margin: 0,*/}
              {/*                    alignSelf: 'flex-start',*/}
              {/*                  },*/}
              {/*                ]}>*/}
              {/*                #{item ? item.billNumber : ''}*/}
              {/*              </Text>*/}
              {/*            </View>*/}
              {/*            <View style={{margin: 0}}>*/}
              {/*              <Text*/}
              {/*                style={[*/}
              {/*                  Styles.button_font,*/}
              {/*                  {*/}
              {/*                    color: COLOR.WHITE,*/}
              {/*                    margin: 0,*/}
              {/*                    alignSelf: 'flex-end',*/}
              {/*                  },*/}
              {/*                ]}>*/}
              {/*                {item ? item.type : ''}*/}
              {/*              </Text>*/}
              {/*              <Text*/}
              {/*                style={[*/}
              {/*                  Styles.small_label,*/}
              {/*                  {margin: 0, color: COLOR.WHITE},*/}
              {/*                ]}>*/}
              {/*                {getDateFromTimeStamp(*/}
              {/*                  item ? item.createdAt : '',*/}
              {/*                  'MMM DD, YYYY',*/}
              {/*                )}*/}
              {/*              </Text>*/}
              {/*            </View>*/}
              {/*          </View>*/}
              {/*        </View>*/}
              {/*      </TouchableWithoutFeedback>*/}
              {/*    )}*/}
              {/*    showsVerticalScrollIndicator={false}*/}
              {/*    showsHorizontalScrollIndicator={false}*/}
              {/*    keyExtractor={(item, index) => index.toString()}*/}
              {/*  />*/}
              {/*</View>*/}
              <View style={[Styles.shadow_view, style.shadowView]}>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}>
                  <View style={style.date_yellow_view}>
                    <Text
                      style={[
                        Styles.small_label,
                        {marginHorizontal: 15, color: COLOR.BLACK},
                      ]}>
                      {Locale('History from all campaigns')}
                    </Text>
                  </View>
                </View>
                <FlatList
                  style={style.flatlist_view}
                  data={this.props.creditRedeemHistoryData}
                  renderItem={({item, index}) => (
                    <TouchableWithoutFeedback
                      onPress={() =>
                        this.props.navigation.navigate('CreditHistoryDetails', {
                          data: item,
                        })
                      }
                      style={{justifyContent: 'center', alignItems: 'center'}}>
                      <View
                        style={[
                          Styles.shadow_view,
                          {
                            width: '100%',
                            marginBottom: 5,
                          },
                        ]}>
                        <View
                          style={{
                            marginBottom: 10,
                            marginHorizontal: 0,
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'space-between',
                          }}>
                          <View style={{margin: 0, flex: 1}}>
                            <Text
                              style={[
                                Styles.button_font,
                                {
                                  color: COLOR.WHITE,
                                  margin: 0,
                                  alignSelf: 'flex-start',
                                },
                              ]}>
                              {insertComma(item.amount ? item.amount : '')} THB
                            </Text>
                            <Text
                              style={[
                                Styles.small_label,
                                {
                                  color: COLOR.WHITE,
                                  margin: 0,
                                  alignSelf: 'flex-start',
                                },
                              ]}>
                              #{item.billNumber}
                            </Text>
                          </View>
                          <View style={{margin: 0}}>
                            <Text
                              style={[
                                Styles.button_font,
                                {
                                  color: COLOR.WHITE,
                                  margin: 0,
                                  alignSelf: 'flex-end',
                                },
                              ]}>
                              {item.type}
                            </Text>
                            <Text
                              style={[
                                Styles.small_label,
                                {margin: 0, color: COLOR.WHITE},
                              ]}>
                              {getDateFromTimeStamp(
                                item.createdAt,
                                'MMM DD, YYYY',
                              )}
                            </Text>
                          </View>
                        </View>
                      </View>
                    </TouchableWithoutFeedback>
                  )}
                  showsVerticalScrollIndicator={false}
                  showsHorizontalScrollIndicator={false}
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>
            </KeyboardAwareScrollView>
            {/*<View*/}
            {/*  style={{width: wp(90), alignSelf: 'center', marginVertical: 10}}>*/}
            {/*  <CustomButton text={'Send text file to email'} />*/}
            {/*</View>*/}
            <ActionSheet
              modalVisible={this.state.actionModalVisible}
              handleSheet={this.handleSheet}
            />
          </SafeAreaView>
        </LinearGradient>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadowView: {
    backgroundColor: COLOR.MEDIUM_BLUE,
    borderRadius: 5,
    width: wp(90),
    alignSelf: 'center',
    marginVertical: 20,
    paddingVertical: 15,
  },
  date_yellow_view: {
    height: 25,
    borderTopRightRadius: 15,
    borderBottomRightRadius: 15,
    marginLeft: 0,
    backgroundColor: COLOR.WHITE,
    justifyContent: 'center',
    alignSelf: 'flex-start',
  },
  flatlist_view: {
    marginVertical: 0,
    width: wp(90),
    marginHorizontal: 15,
    alignSelf: 'center',
    backgroundColor: COLOR.MEDIUM_BLUE,
    borderRadius: 5,
    paddingVertical: 15,
    paddingHorizontal: 10,
  },
  icon_image: {
    height: 40,
    width: 40,
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    userDetail: state.authReducer.userInfo,
    campaignData: state.campaignReducer.campaignData,
    creditRedeemHistoryData: state.campaignReducer.creditRedeemHistoryData,
    creditRequestHistoryData: state.campaignReducer.creditRequestHistoryData,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getUserInfo: val => {
      dispatch(AuthenticationAction.getUserDetail(val));
    },
    getCampaignData: val => {
      dispatch(CampaignActions.getCampaignData(val));
    },
    getCreditRedeemHistoryData: (val, id) => {
      dispatch(CampaignActions.getCreditRedeemHistoryData(val, id));
    },
    getCreditRequestHistoryData: val => {
      dispatch(CampaignActions.getCreditRequestHistoryData(val));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RedeemCreditScreen);
