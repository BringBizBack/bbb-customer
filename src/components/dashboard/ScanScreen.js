import React from 'react';
import {
  View,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  Text,
  Platform,
} from 'react-native';
import Styles from '../../styles/Styles';
import COLOR from '../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import FONTS from '../../styles/Fonts';
import NavigationBar from '../../commonView/NavigationBar';
import LinearGradient from 'react-native-linear-gradient';
import AuthenticationAction from '../../redux/action/AuthenticationAction';
import {connect} from 'react-redux';
import QRCode from 'react-native-qrcode-svg';
import {
  getDateFromTimeStamp,
  getTimestampFromDate,
  handleDispatch,
  Locale,
  showAlertMessage,
} from '../../commonView/Helpers';
import API from '../../api/Api';
import AlertPopup from '../../commonView/AlertPopup';
import IMAGES from '../../styles/Images';
import CampaignActions from '../../redux/action/CampaignActions';
import {CustomButton} from '../../commonView/CustomButton';
import ActivityIndicator from '../../commonView/ActivityIndicator';
import {Dropdown} from '../../commonView/Dropdown';

let api = new API();

class ScanScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      campaignDetail: null,
      type: null,
      alertModalVisible: false,
      isLoading: false,
      selectedServer: null,
      selectedCampaign: null,
      serverDropdownData: null,
    };
    this.updateMasterState = this.updateMasterState.bind(this);
    this.callRequestAPI = this.callRequestAPI.bind(this);
    this.closeVerifyModal = this.closeVerifyModal.bind(this);
    this.handleDropdown = this.handleDropdown.bind(this);
    this.changeState = this.changeState.bind(this);
    this.getDropdownData = this.getDropdownData.bind(this);
  }

  componentDidMount() {
    this.props.getUserDetail();
    if (this.props.route.params) {
      if (this.props.route.params.data) {
        this.setState(
          {
            campaignDetail: this.props.route.params.data,
            type: this.props.route.params.type,
            selectedCampaign: this.props.route.params.campaign
              ? this.props.route.params.campaign
              : null,
          },
          () => {
            this.getDropdownData();
          },
        );
      }
    }
  }

  updateMasterState = props => {};

  commonView = props => {
    return (
      <View
        style={{
          flexDirection: 'row',
          width: '90%',
          alignSelf: 'center',
          marginTop: 10,
        }}>
        <Text
          style={[Styles.small_label, {width: 110, alignSelf: 'flex-start'}]}>
          {props.attr}
        </Text>
        <Text
          style={[
            Styles.body_label,
            {flex: 1, alignSelf: 'flex-start', color: COLOR.DARK_BLUE},
          ]}>
          {props.value}
        </Text>
      </View>
    );
  };

  callRequestAPI() {
    this.setState({isLoading: true});

    api
      .requestCredit(this.state.campaignDetail, this.state.type)
      .then(json => {
        this.setState({isLoading: false});
        if (json.status == 200) {
          if (json.data.status == 200) {
            this.props.getCreditRedeemHistoryData(1);
            this.props.getCreditRequestHistoryData(1);
            this.setState({alertModalVisible: true});
          } else {
            showAlertMessage(json.data.message);
          }
        } else if (json.status == 400) {
          showAlertMessage(json.data.message, 2);
        } else {
          showAlertMessage(json.data.message, 2);
        }
      })
      .catch(error => {
        this.setState({isLoading: false});
        if (error.response.data) {
          showAlertMessage(error.response.data.message, 2);
        }
      });
  }

  closeVerifyModal = () => {
    this.setState({alertModalVisible: false}, () => {
      this.setState({
        amountRequested: '',
        billNumber: '',
        billImage: null,
        memberCount: 1,
      });
      if (this.state.type == 1) {
        this.props.navigation.navigate('LeaderboardScreen', {
          tabScreen: true,
          data: this.state.selectedCampaign,
          redirectionFromLogin: false,
        });
      }
    });
  };

  changeState = (val, camp) => {
    this.setState({
      selectedServer: val,
      campaignDetail: camp,
    });
  };

  handleDropdown(val, type) {
    if (this.state.selectedCampaign && this.state.selectedCampaign.members) {
      let selectedValue = this.state.selectedCampaign.members.find(
        data => (data.firstName ?? '') + ' ' + (data.lastName ?? '') === val,
      );

      if (selectedValue) {
        let campaign = this.state.campaignDetail;
        campaign.server = selectedValue.id;
        this.setState({
          selectedServer: selectedValue,
          campaignDetail: campaign,
        });
        this.changeState(selectedValue, campaign);
      }
    }
  }

  getDropdownData = () => {
    let data = this.state.selectedCampaign
      ? this.state.selectedCampaign.members
        ? this.state.selectedCampaign.members.map(val => {
            return {
              id: val.id,
              value: val.firstName + ' ' + val.lastName,
              label: val.firstName + ' ' + val.lastName,
            };
          })
        : []
      : [];

    this.setState({
      serverDropdownData: data,
    });
  };

  render() {
    let {campaignDetail} = this.state;
    let base64Logo = 'test';

    return (
      <View style={Styles.container}>
        <LinearGradient
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
          colors={[COLOR.TOP_GRADIENT, COLOR.BOTTOM_GRADIENT]}
          style={Styles.container}>
          <AlertPopup
            image={IMAGES.security}
            title={
              this.state.type
                ? Locale('REDEEM REQUEST SENT')
                : Locale('CREDIT REQUEST SENT')
            }
            buttonText={'Home'}
            prop={this.props}
            desc={`Business: ${
              this.state.campaignDetail
                ? this.state.campaignDetail.businessName
                : ''
            }\n${Locale('Amount:')} ${
              this.state.campaignDetail ? this.state.campaignDetail.amount : 0
            } THB`}
            closeVerifyModal={() => this.closeVerifyModal()}
            modalVisible={this.state.alertModalVisible}
          />
          <ActivityIndicator loading={this.state.isLoading} />
          <SafeAreaView style={Styles.container}>
            <NavigationBar />
            <ScrollView>
              <View
                style={[
                  style.shadowView,
                  Styles.shadow_view,
                  {marginTop: 20, marginBottom: 0},
                ]}>
                <View
                  style={{
                    alignItems: 'center',
                    alignSelf: 'center',
                    margin: 10,
                    marginVertical: 20,
                  }}>
                  <QRCode
                    value={
                      this.props.userDetail
                        ? `${this.props.userDetail.email},${
                            campaignDetail
                              ? campaignDetail.amount.replace(',', '')
                              : ''
                          },${this.state.type},${
                            campaignDetail ? campaignDetail.campaign : ''
                          },${
                            campaignDetail
                              ? campaignDetail.billNumber.replace(',', '')
                              : ''
                          },${campaignDetail ? campaignDetail.bill : ''},${
                            campaignDetail ? campaignDetail.visitors : ''
                          }`
                        : 'test'
                    }
                    logo={{uri: base64Logo}}
                    logoBackgroundColor="transparent"
                  />
                </View>
                {/*<Text*/}
                {/*  style={{*/}
                {/*    fontFamily: FONTS.FAMILY_SEMIBOLD,*/}
                {/*    fontWeight: '600',*/}
                {/*    fontSize: 30,*/}
                {/*    alignSelf: 'center',*/}
                {/*  }}>*/}
                {/*  {campaignDetail ? campaignDetail.billNumber : ''}*/}
                {/*</Text>*/}
                <Text
                  style={[
                    Styles.small_label,
                    {
                      alignSelf: 'center',
                      width: wp(50),
                      textAlign: 'center',
                      marginTop: 10,
                      fontFamily: FONTS.FAMILY_SEMIBOLD,
                    },
                  ]}>
                  {Locale('Scan QR or press submit to proceed forward')}
                </Text>
              </View>
              <View
                style={[style.shadowView, Styles.shadow_view, {marginTop: 0}]}>
                <this.commonView
                  attr={'Trade Name'}
                  value={
                    this.state.selectedCampaign
                      ? this.state.selectedCampaign.trading
                        ? this.state.selectedCampaign.trading.name
                        : ''
                      : ''
                  }
                />
                <this.commonView
                  attr={Locale('Campaign Name')}
                  value={
                    this.state.selectedCampaign
                      ? this.state.selectedCampaign.campaignTemplate
                        ? this.state.selectedCampaign.campaignTemplate.name
                        : ''
                      : ''
                  }
                />

                <this.commonView
                  attr={'Campaign end date'}
                  value={getDateFromTimeStamp(
                    this.state.selectedCampaign
                      ? this.state.selectedCampaign
                        ? this.state.selectedCampaign.endDate
                        : ''
                      : '',
                    'MMM DD, YYYY',
                  )}
                />

                <this.commonView
                  attr={Locale('Amount')}
                  value={campaignDetail ? campaignDetail.amount + ' THB' : ''}
                />

                <this.commonView
                  attr={Locale('Visitor in group')}
                  value={campaignDetail ? campaignDetail.visitors : ''}
                />
                <this.commonView
                  attr={Locale('Bill number')}
                  value={campaignDetail ? campaignDetail.billNumber : ''}
                />
                <View
                  style={[Styles.line_view, {width: '90%', marginVertical: 15}]}
                />
                <this.commonView
                  attr={Locale('Name')}
                  value={
                    this.props.userDetail
                      ? (this.props.userDetail.firstName ?? '') +
                        ' ' +
                        (this.props.userDetail.lastName ?? '')
                      : ''
                  }
                />
                <this.commonView
                  attr={Locale('Email')}
                  value={
                    this.props.userDetail ? this.props.userDetail.email : ''
                  }
                />
                <this.commonView
                  attr={Locale('Customer ID')}
                  value={
                    this.props.userDetail ? this.props.userDetail.bbb_id : ''
                  }
                />
              </View>

              {/*<View*/}
              {/*  style={[*/}
              {/*    style.shadowView,*/}
              {/*    Styles.shadow_view,*/}
              {/*    {*/}
              {/*      backgroundColor: COLOR.LIGHT_BLUE,*/}
              {/*      marginTop: 0,*/}
              {/*      ...(Platform.OS !== 'android' && {*/}
              {/*        zIndex: 1000,*/}
              {/*      }),*/}
              {/*    },*/}
              {/*  ]}>*/}
              {/*  <View*/}
              {/*    style={{*/}
              {/*      height: 45,*/}

              {/*      marginHorizontal: 15,*/}
              {/*      ...(Platform.OS !== 'android' && {*/}
              {/*        zIndex: 1000,*/}
              {/*      }),*/}
              {/*    }}>*/}
              {/*    <Dropdown*/}
              {/*      placeholder={Locale('Select server (optional)')}*/}
              {/*      type={1}*/}
              {/*      items={this.state.serverDropdownData ?? []}*/}
              {/*      value={*/}
              {/*        this.state.selectedServer ? this.state.selectedServer : ''*/}
              {/*      }*/}
              {/*      zIndex={1000}*/}
              {/*      handleDropdown={this.handleDropdown}*/}
              {/*    />*/}
              {/*  </View>*/}
              {/*</View>*/}
              {/*<View style={{width: wp(90), alignSelf: 'center'}}>*/}
              {/*  <CustomButton*/}
              {/*    text={Locale('Submit')}*/}
              {/*    onPress={this.callRequestAPI}*/}
              {/*  />*/}
              {/*</View>*/}
            </ScrollView>
          </SafeAreaView>
        </LinearGradient>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadowView: {
    backgroundColor: COLOR.WHITE,
    borderRadius: 10,
    width: wp(90),
    alignSelf: 'center',
    marginVertical: 20,
    paddingVertical: 15,
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    userDetail: state.authReducer.userInfo,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getUserDetail: val => {
      dispatch(AuthenticationAction.getUserDetail(val));
    },
    getCreditRedeemHistoryData: val => {
      dispatch(CampaignActions.getCreditRedeemHistoryData(val));
    },
    getCreditRequestHistoryData: val => {
      dispatch(CampaignActions.getCreditRequestHistoryData(val));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ScanScreen);
