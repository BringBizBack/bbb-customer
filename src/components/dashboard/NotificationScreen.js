import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Image,
  FlatList,
} from 'react-native';
import Styles from '../../styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';
import COLOR from '../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';
import AuthenticationAction from '../../redux/action/AuthenticationAction';
import {connect} from 'react-redux';
import {Locale, navigate} from '../../commonView/Helpers';
import IMAGES from '../../styles/Images';

class NotificationScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      campaignData: [],
    };

    this.updateMasterState = this.updateMasterState.bind(this);
    this.getNotificationImage = this.getNotificationImage.bind(this);
  }

  componentDidMount() {
    this.props.getUserNotification(1);
  }

  updateMasterState = (props, value) => {
    this.setState({
      [props]: value,
    });
  };

  noNotificationMessage(msg) {
    return (
      <View
        style={{
          alignItems: 'center',
          marginHorizontal: 10,
          width: wp(85),
          alignSelf: 'center',
        }}>
        <Text
          style={[
            Styles.bold_body_label,
            {
              color: COLOR.WHITE,
              textAlign: 'left',
              multiline: true,
              marginTop: 100,
            },
          ]}>
          {Locale('No notification(s) data!')}
        </Text>
      </View>
    );
  }

  getNotificationImage(type, redirect) {
    switch (String(type)) {
      //5: Add Credit (From Business End)
      case '5':
        if (redirect) {
          navigate('RedeemCreditScreen');
        } else {
          return IMAGES.add_yellow;
        }
        break;
      //6: Redeem Credit (From Business End)
      case '6':
        if (redirect) {
          navigate('MyTabs');
        } else {
          return IMAGES.add_yellow;
        }
        break;
      //  9: Credit Approved
      case '9':
        if (redirect) {
          navigate('MyTabs');
        } else {
          return IMAGES.add_yellow;
        }
        break;
      //  10: Redeem Credit Approved
      case '10':
        if (redirect) {
          navigate('MyTabs');
        } else {
          return IMAGES.add_yellow;
        }
        break;
      //  11: New Campaign Started
      case '11':
        if (redirect) {
          navigate('ExploreCampaign', {tabScreen: true});
        } else {
          return IMAGES.add_yellow;
        }
        break;
      //  13:  Campaign Ended(Customer)
      case '13':
        if (redirect) {
          navigate('MyTabs');
        } else {
          return IMAGES.add_yellow;
        }
        break;
      default:
        if (redirect) {
          //navigate('MyTabs');
        } else {
          return IMAGES.add_yellow;
        }
        break;
    }
  }

  render() {
    return (
      <View style={Styles.container}>
        <LinearGradient
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
          colors={[COLOR.TOP_GRADIENT, COLOR.BOTTOM_GRADIENT]}
          style={Styles.container}>
          <SafeAreaView style={Styles.container}>
            <NavigationBar title={Locale('Notifications')} />
            <FlatList
              style={style.flatlist_view}
              data={this.props.notificationData}
              renderItem={({item, index}) => (
                <TouchableWithoutFeedback
                  onPress={() => this.getNotificationImage(item.type, true)}
                  style={{justifyContent: 'center', alignItems: 'center'}}>
                  <View
                    style={[
                      Styles.shadow_view,
                      style.shadowView,
                      {
                        width: '100%',
                        marginBottom: 10,
                      },
                    ]}>
                    <View
                      style={{
                        marginBottom: 0,
                        marginHorizontal: 0,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                      }}>
                      <Image
                        style={{height: 30, width: 30, alignSelf: 'flex-start'}}
                        source={this.getNotificationImage(item.type)}
                        resizeMode={'cover'}
                      />
                      <View style={{margin: 0, flex: 1, marginLeft: 15}}>
                        <Text
                          style={[
                            Styles.bold_body_label,
                            {
                              color: COLOR.WHITE,
                              margin: 0,
                              alignSelf: 'flex-start',
                            },
                          ]}>
                          {item.title}
                        </Text>
                        <Text
                          style={[
                            Styles.extra_small_label,
                            {
                              color: COLOR.WHITE,
                              margin: 0,
                              marginTop: 5,
                              alignSelf: 'flex-start',
                            },
                          ]}>
                          {item.message}
                        </Text>
                      </View>
                    </View>
                  </View>
                </TouchableWithoutFeedback>
              )}
              showsVerticalScrollIndicator={false}
              showsHorizontalScrollIndicator={false}
              keyExtractor={(item, index) => index.toString()}
              ListHeaderComponent={
                this.props.notificationData
                  ? this.props.notificationData.length > 0
                    ? null
                    : this.noNotificationMessage()
                  : this.noNotificationMessage()
              }
            />
          </SafeAreaView>
        </LinearGradient>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadowView: {
    backgroundColor: COLOR.LIGHT_BLUE,
    borderRadius: 10,
    padding: 15,
    width: wp(90),
    alignSelf: 'center',
  },
  flatlist_view: {
    marginVertical: 0,
    width: wp(90),
    marginHorizontal: 15,
    alignSelf: 'center',
    borderRadius: 5,
    paddingVertical: 15,
    paddingHorizontal: 10,
  },
  date_yellow_view: {
    height: 30,
    borderTopRightRadius: 15,
    borderBottomRightRadius: 15,
    marginTop: 10,
    marginLeft: 0,
    backgroundColor: COLOR.YELLOW,
    justifyContent: 'center',
    alignSelf: 'flex-start',
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    notificationData: state.authReducer.userNotification,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getUserNotification: val => {
      dispatch(AuthenticationAction.getNotifications(val));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NotificationScreen);
