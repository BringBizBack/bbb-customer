/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import 'react-native-gesture-handler';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';

import LoginScreen from '../../components/registration/LoginScreen';
import AboutScreen from '../../components/registration/AboutScreen';
import ExploreCampaign from '../campaign/ExploreCampaign';
import CampaignDetail from '../campaign/CampaignDetail';
import LeaderboardScreen from '../campaign/LeaderboardScreen';
import PrizeDetailScreen from '../campaign/PrizeDetailScreen';
import BusinessDetail from '../campaign/BusinessDetail';
import CampaignRules from '../campaign/CampaignRules';
import SignupScreen from '../registration/SignupScreen';
import VerifyEmail from '../registration/VerifyEmail';
import DashboardScreen from '../dashboard/homeTab/DashboardScreen';
import ScanScreen from '../dashboard/ScanScreen';
import RedeemCreditScreen from '../dashboard/RedeemCreditScreen';
import NotificationScreen from '../dashboard/NotificationScreen';

import VerifyIdScreen from '../dashboard/homeTab/VerifyIdScreen';
import SplashScreen from '../splash/SplashView';
import ForgotPasswordScreen from '../registration/ForgotPassswordScreen';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import IMAGES from '../../styles/Images';
import {StyleSheet, View, TouchableOpacity, Image, SafeAreaView, Text, Alert} from 'react-native';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import Styles from '../../styles/Styles';
import SuccessScreen from '../dashboard/homeTab/SuccessScreen';
import SettingsScreen from '../dashboard/homeTab/SettingsScreen';
import InitialLoadingScreen from '../splash/InitialLoadingScreen';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {AS_DEFAULT_HOME_SCREEN, AS_INITIAL_ROUTE, AS_USER_DETAIL} from '../../commonView/Constants';
import Auth from '../../auth';
import CreditHistoryDetails from '../history/CreditHistoryDetails';
import TermsConditionScreen from '../registration/TermsConditionScreen';
import PrivacyPolicyScreen from '../registration/PrivacyPolicyScreen';
import {setNavigationRef} from '../../commonView/Helpers';
import CampaignActions from '../../redux/action/CampaignActions';
import {connect, useSelector, useDispatch} from 'react-redux';
import store from '../../redux/Store';
import AuthenticationAction from '../../redux/action/AuthenticationAction';
import AddCampaignDetail from '../campaign/AddCampaignDetail';

let auth = new Auth();
const Tab = createBottomTabNavigator();
const HomeStack = createStackNavigator();
const CreditStack = createStackNavigator();
const CampaignStack = createStackNavigator();
const Stack = createStackNavigator();

const images = [
  IMAGES.home,
  IMAGES.plus_circle,
  IMAGES.campaign,
];

const titles = [
  'Home',
  'Request points',
  'Campaigns',
];

export function MyTabBar({ state, descriptors, navigation }) {
  return (
    <View style={style.bottom_tab_container}>

      {state.routes.map((route, index) => {
        const { options } = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
              ? options.title
              : route.name;
        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
            <SafeAreaView style={{flex:1,backgroundColor: COLOR.DARK_BLUE}}>
          <TouchableOpacity
            accessibilityRole="button"
            accessibilityStates={isFocused ? ['selected'] : []}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}>
            <View style={{
              flexDirection:'row',alignItems:'center', justifyContent: 'space-between'}}>
            <View style={{marginVertical:5,flex:1}}>
              <Image
                style={{
                  width: 25,
                  height: 25,
                  alignItems: 'center',
                  alignSelf: 'center',
                  tintColor: isFocused ? COLOR.YELLOW : COLOR.WHITE,
                }}
                resizeMode={'cover'}
                source={images[index]}
              />
              <Text style={[Styles.small_label,{ marginTop:5,color: isFocused ? COLOR.YELLOW : COLOR.WHITE, textAlign:'center'}]}>{titles[index]}</Text>
            </View>
              {index != 2 && <View style={{width: 1.5, height: 30, backgroundColor: COLOR.WHITE}}/>}
            </View>
          </TouchableOpacity>
            </SafeAreaView>
        );
      })}
    </View>

        );
}

const MyTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: '#ffffff',
  },
};

export default function RootStack(rootProps) {
  return (
    <NavigationContainer ref={ref=>{
        setNavigationRef(ref);
    }} theme={MyTheme}>
      <Stack.Navigator  screenOptions={{ gestureEnabled: false }} initialRouteName={'InitialLoadingScreen'} headerMode="none">
          <Stack.Screen name="InitialLoadingScreen"component={InitialLoadingScreen}/>
          <Stack.Screen name="SplashScreen"component={SplashScreen}/>
        <Stack.Screen name="LoginScreen"component={LoginScreen}/>
        <Stack.Screen name="ForgotPasswordScreen"component={ForgotPasswordScreen}/>
        <Stack.Screen name="AboutScreen"component={AboutScreen}/>
        <Stack.Screen name="ExploreCampaign"component={ExploreCampaign} initialParams={{tabScreen:true}}/>
        <Stack.Screen name="CampaignDetail"component={CampaignDetail}/>
        <Stack.Screen name="LeaderboardScreen"component={LeaderboardScreen}/>
        <Stack.Screen name="PrizeDetailScreen"component={PrizeDetailScreen}/>
        <Stack.Screen name="BusinessDetail"component={BusinessDetail}/>
        <Stack.Screen name="CampaignRules"component={CampaignRules}/>
        <Stack.Screen name="SignupScreen"component={SignupScreen}/>
        <Stack.Screen name="VerifyIdScreen"component={VerifyIdScreen} initialParams={{tabScreen:true}}/>
        <Stack.Screen name="VerifyEmail"component={VerifyEmail}/>
        <Stack.Screen name="DashboardScreen"component={DashboardScreen}/>
        <Stack.Screen name="ScanScreen"component={ScanScreen}/>
        <Stack.Screen name="RedeemCreditScreen"component={RedeemCreditScreen}/>
        <Stack.Screen name="CreditHistoryDetails"component={CreditHistoryDetails}/>
        <Stack.Screen name="NotificationScreen"component={NotificationScreen}/>
        <Stack.Screen name="AddCampaignDetail"component={AddCampaignDetail}/>

        <Stack.Screen name="SuccessScreen"component={SuccessScreen}/>
        <Stack.Screen name="MyTabs"component={MyTabs} />
        <Stack.Screen name="TermsConditionScreen"component={TermsConditionScreen}/>
        <Stack.Screen name="PrivacyPolicyScreen"component={PrivacyPolicyScreen}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}

function MyTabs(props) {

  return (
      <Tab.Navigator
          initialRouteName={'CreditScreen'}
          tabBar={props => <MyTabBar {...props} />}>
        <Tab.Screen key={1} name="Home" component={HomeScreen} initialParams={{tabScreen:true}} />
           <Tab.Screen key={2} name="Credit Screen" component={CreditScreen}/>
        <Tab.Screen key={3} name="Campaigns" component={CampaignScreen}  />
      </Tab.Navigator>
  );
}



function HomeScreen() {
  return (
      <HomeStack.Navigator screenOptions={{ gestureEnabled: false }} initialRouteName={ 'DashboardScreen'} headerMode="none">
        <Stack.Screen name="DashboardScreen"component={DashboardScreen} initialParams={{tabScreen:true}}/>
        <Stack.Screen name="SuccessScreen"component={SuccessScreen} options={{animationEnabled: false}} initialParams={{tabScreen:true}}/>
        <Stack.Screen name="SettingsScreen"component={SettingsScreen} initialParams={{tabScreen:true}}/>
          <Stack.Screen name="AboutScreen" component={AboutScreen} initialParams={{tabScreen:true}}/>
      </HomeStack.Navigator>
  );
}

function CreditScreen(props) {

    return (
        <CreditStack.Navigator screenOptions={{ gestureEnabled: false }} initialRouteName={'RedeemCreditScreen'} headerMode="none">
            <Stack.Screen name="RedeemCreditScreen" component={RedeemCreditScreen} initialParams={{tabScreen: true}}/>
        </CreditStack.Navigator>
    );
}


function CampaignScreen(){
    const routeName =  AsyncStorage.getItem(AS_USER_DETAIL);
    return (
        <CampaignStack.Navigator screenOptions={{ gestureEnabled: false }} initialRouteName={'ExploreCampaign'} headerMode="none">
            <Stack.Screen name="ExploreCampaign" component={ExploreCampaign} initialParams={{tabScreen:true}}/>
            <Stack.Screen name="CampaignRules" component={CampaignRules} initialParams={{tabScreen:true}}/>
            <Stack.Screen name="PrizeDetailScreen" component={PrizeDetailScreen} initialParams={{tabScreen:true}}/>
        </CampaignStack.Navigator>
    );
}

const style = StyleSheet.create({
  bottom_tab_container: {
    flexDirection: 'row',
    alignSelf: 'center',
    width: widthPercentageToDP(100),
    margin: 0,
    justifyContent: 'center',
    backgroundColor: COLOR.DARK_BLUE,
    alignItems: 'center',
  },

});
