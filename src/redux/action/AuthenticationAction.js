import types from '../../redux/Types';
import {AS_USER_DETAIL} from '../../commonView/Constants';
import API from '../../api/Api';
import Auth from '../../auth/index';
import {showAlertMessage} from '../../commonView/Helpers';
import {Alert} from 'react-native';

const api = new API();
const auth = new Auth();

const getUserInfo = () => {
  return async (dispatch, getStore) => {
    let userInfo = await getStore().authReducer.userDetail;

    if (!userInfo) {
      try {
        let userData = await auth.getValue(AS_USER_DETAIL);

        dispatch({
          type: types.USER_DETAIL,
          data: JSON.parse(userData),
        });
      } catch (error) {
        console.log('error getting user detail');
      }
    } else {
      return dispatch({
        type: types.USER_DETAIL,
        data: userInfo,
      });
    }
  };
};

const getUserDetail = val => {
  return async (dispatch, getStore) => {
    let userDetail = await getStore().authReducer.userInfo;

    if (!userDetail || val == 1) {
      try {
        api
          .getUserDetail()
          .then(json => {
            console.log('user detail is:-', json);
            if (json.data.status == 200) {
              dispatch({
                type: types.USER_INFO,
                data: json.data.response,
              });
            } else {
              showAlertMessage(json.data.message);
            }
          })
          .catch(function (error) {
            showAlertMessage(error.response.data.message);
          });
      } catch (error) {
        console.log('error getting user business detail');
      }
    } else {
      return dispatch({
        type: types.USER_INFO,
        data: userDetail,
      });
    }
  };
};

const saveUserDetail = data => {
  console.log('user detail data is:-', data);
  return async (dispatch, getStore) => {
    try {
      await auth.setValue(AS_USER_DETAIL, data);
      await dispatch({type: types.USER_DETAIL, data: JSON.parse(data)});
    } catch (error) {
      console.log(error.message);
    }
  };
};

const getNotifications = val => {
  return async (dispatch, getStore) => {
    let notificationData = await getStore().authReducer.userNotification;
    if (!notificationData || val == 1) {
      try {
        api
          .getUserNotification()
          .then(json => {
            console.log('notification data is:-', json);
            if (json.data.status == 200) {
              console.log('json data.response', json.data.response);
              dispatch({
                type: types.USER_NOTIFICATION,
                data: json.data.response,
              });
            } else {
              showAlertMessage(json.data.message);
            }
          })
          .catch(function (error) {
            if (
              error.response.data.message !=
              'Please complete your profile first'
            ) {
              showAlertMessage(error.response.data.message);
            }
          });
      } catch (error) {
        console.log('error getting user business detail');
      }
    } else {
      return dispatch({
        type: types.USER_NOTIFICATION,
        data: notificationData,
      });
    }
  };
};

const logoutUser = () => {
  return async (dispatch, getStore) => {
    return dispatch({
      type: types.USER_LOGOUT,
      data: null,
    });
  };
};

export default {
  getUserInfo,
  logoutUser,
  saveUserDetail,
  getUserDetail,
  getNotifications,
};
