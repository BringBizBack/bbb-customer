import types from '../Types';
import {showAlertMessage} from '../../commonView/Helpers';
import API from '../../api/Api';
import {Alert} from 'react-native';

let api = new API();

const getCampaignData = (val, id, sort) => {
  return async (dispatch, getStore) => {
    let campaignDetail = await getStore().campaignReducer.campaignData;

    if (!campaignDetail || val == 1) {
      try {
        let data = JSON.stringify({
          nation: 'Thailand',
          city: id,
          sort: sort,
        });

        api
          .getCampaign(data)
          .then(json => {
            console.log('campaign data is:-', json);
            if (json.data.status == 200) {
              return dispatch({
                type: types.CAMPAIGN_DATA,
                data: json.data.response,
              });
            } else {
              showAlertMessage(json.data.message);
            }
          })
          .catch(function (error) {
            showAlertMessage(error.response.data.message);
          });
      } catch (error) {
        console.log('error getting campaign detail');
      }
    } else {
      return dispatch({
        type: types.CAMPAIGN_DATA,
        data: campaignDetail,
      });
    }
  };
};

const getMyWonGoalprize = val => {
  return async (dispatch, getStore) => {
    let creditData = await getStore().campaignReducer.myCreditDate;

    if (!creditData || val == 1) {
      try {
        api
          .getMyWonGoalPrize()
          .then(json => {
            console.log('campaign data is:-', json);
            if (json.data.status == 200) {
              return dispatch({
                type: types.GET_WON_GOALPRIZE_DATA,
                data: json.data.response,
              });
            } else {
              showAlertMessage(json.data.message);
            }
          })
          .catch(function (error) {
            showAlertMessage(error.response.data.message);
          });
      } catch (error) {
        console.log('error getting campaign detail');
      }
    } else {
      return dispatch({
        type: types.GET_WON_GOALPRIZE_DATA,
        data: creditData,
      });
    }
  };
};

const getLostCampaignData = val => {
  return async (dispatch, getStore) => {
    let lostData = await getStore().campaignReducer.lostCampaignData;
    if (!lostData || val == 1) {
      try {
        api
          .getLostCampaign()
          .then(json => {
            console.log('campaign data is:-', json);
            if (json.data.status == 200) {
              return dispatch({
                type: types.LOST_CAMPAIGN_DATA,
                data: json.data.response,
              });
            } else {
              showAlertMessage(json.data.message);
            }
          })
          .catch(function (error) {
            showAlertMessage(error.response.data.message);
          });
      } catch (error) {
        console.log('error getting campaign detail');
      }
    } else {
      return dispatch({
        type: types.LOST_CAMPAIGN_DATA,
        data: lostData,
      });
    }
  };
};

const getMyParticipatedCampaign = val => {
  return async (dispatch, getStore) => {
    let campaignData = await getStore().campaignReducer
      .participatedCampaignData;
    if (!campaignData || val == 1) {
      try {
        api
          .getMyCurrentParticipatedCampaign()
          .then(json => {
            console.log('my participate campaign data is:-', json);
            if (json.data.status == 200) {
              return dispatch({
                type: types.GET_PARTICIPATED_CAMPAIGN_DATA,
                data: json.data.response,
              });
            } else {
              showAlertMessage(json.data.message);
            }
          })
          .catch(function (error) {
            showAlertMessage(error.response.data.message);
          });
      } catch (error) {
        console.log('error getting campaign detail');
      }
    } else {
      return dispatch({
        type: types.GET_PARTICIPATED_CAMPAIGN_DATA,
        data: campaignData,
      });
    }
  };
};

const getCreditRedeemHistoryData = (val, id) => {
  return async (dispatch, getStore) => {
    let historyData = await getStore().campaignReducer.creditRedeemHistoryData;
    if (!historyData || val == 1) {
      try {
        api
          .getCreditRedeemHistory({campaign: id})
          .then(json => {
            console.log('history data is:-', json);
            if (json.data.status == 200) {
              return dispatch({
                type: types.REDEEM_CREDIT_DATA,
                data: json.data.response,
              });
            } else {
              showAlertMessage(json.data.message);
            }
          })
          .catch(function (error) {
            showAlertMessage(error.response.data.message);
          });
      } catch (error) {
        console.log('error getting campaign detail');
      }
    } else {
      return dispatch({
        type: types.REDEEM_CREDIT_DATA,
        data: historyData,
      });
    }
  };
};

const getCreditRequestHistoryData = val => {
  return async (dispatch, getStore) => {
    let historyData = await getStore().campaignReducer.creditRequestHistoryData;
    if (!historyData || val == 1) {
      try {
        api
          .getCreditRequestHistory()
          .then(json => {
            console.log('request history data is:-', json);
            if (json.data.status == 200) {
              return dispatch({
                type: types.REQUEST_CREDIT_DATA,
                data: json.data.response,
              });
            } else {
              showAlertMessage(json.data.message);
            }
          })
          .catch(function (error) {
            showAlertMessage(error.response.data.message);
          });
      } catch (error) {
        console.log('error getting campaign detail');
      }
    } else {
      return dispatch({
        type: types.REQUEST_CREDIT_DATA,
        data: historyData,
      });
    }
  };
};

const getSingleCampaignData = val => {
  return async (dispatch, getStore) => {
    try {
      api
        .getSingleCampaignDetail(val)
        .then(json => {
          console.log('single campaign data is:-', json);
          if (json.data.status == 200) {
            return dispatch({
              type: types.SINGLE_CAMPAIGN_DATA,
              data: json.data.response,
            });
          } else {
            showAlertMessage(json.data.message);
          }
        })
        .catch(function (error) {
          showAlertMessage(error.response.data.message);
        });
    } catch (error) {
      console.log('error getting campaign detail');
    }
  };
};

export default {
  getCampaignData,
  getSingleCampaignData,
  getMyWonGoalprize,
  getMyParticipatedCampaign,
  getCreditRedeemHistoryData,
  getCreditRequestHistoryData,
  getLostCampaignData,
};
