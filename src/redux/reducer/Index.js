import {combineReducers} from 'redux';
import AuthenticationReducer from './AuthenticationReducer';
import CampaignReducer from './CampaignReducer';
import {Alert} from 'react-native';

const appReducer = combineReducers({
  authReducer: AuthenticationReducer,
  campaignReducer: CampaignReducer,
});

const rootReducer = (state, action) => {
  if (action.type === 'USER_LOGOUT') {
    return appReducer(undefined, action);
  }
  return appReducer(state, action);
};

export default rootReducer;
