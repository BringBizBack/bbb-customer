import types from '../Types';
import {Alert} from 'react-native';

let newState = {
  userDetail: null,
  userInfo: null,
  userNotification: null,
};

let cloneObject = function (obj) {
  return JSON.parse(JSON.stringify(obj));
};

export default function reducer(state, action) {
  switch (action.type) {
    case types.USER_DETAIL:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          userDetail: action.data,
        });
      }
      return newState;
    case types.USER_INFO:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          userInfo: action.data,
        });
      }
      return newState;
    case types.USER_NOTIFICATION:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          userNotification: action.data,
        });
      }
      return newState;
    case types.USER_LOGOUT:
      newState = Object.assign({}, newState, {
        userDetail: null,
        userInfo: null,
        userNotification: null,
      });
      return newState;
    default:
      return state || newState;
  }
}
