import types from '../Types';
import {Alert} from 'react-native';

let newState = {
  campaignData: null,
  singleCampaignDetail: null,
  myCreditDate: null,
  participatedCampaignData: null,
  creditRedeemHistoryData: null,
  creditRequestHistoryData: null,
  lostCampaignData: null,
};

let cloneObject = function (obj) {
  return JSON.parse(JSON.stringify(obj));
};

export default function reducer(state, action) {
  switch (action.type) {
    case types.CAMPAIGN_DATA:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          campaignData: action.data,
        });
      }
      return newState;
    case types.SINGLE_CAMPAIGN_DATA:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          singleCampaignDetail: action.data,
        });
      }
      return newState;
    case types.GET_WON_GOALPRIZE_DATA:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          myCreditDate: action.data,
        });
      }
      return newState;
    case types.GET_WON_GOALPRIZE_DATA:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          myCreditDate: action.data,
        });
      }
      return newState;
    case types.GET_WON_GOALPRIZE_DATA:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          myCreditDate: action.data,
        });
      }
      return newState;
    case types.GET_PARTICIPATED_CAMPAIGN_DATA:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          participatedCampaignData: action.data,
        });
      }
      return newState;
    case types.REDEEM_CREDIT_DATA:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          creditRedeemHistoryData: action.data,
        });
      }
      return newState;
    case types.REQUEST_CREDIT_DATA:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          creditRequestHistoryData: action.data,
        });
      }
      return newState;
    case types.LOST_CAMPAIGN_DATA:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          lostCampaignData: action.data,
        });
      }
      return newState;
    case types.USER_LOGOUT:
      newState = Object.assign({}, newState, {
        campaignData: null,
        singleCampaignDetail: null,
        myCreditDate: null,
        participatedCampaignData: null,
        creditRedeemHistoryData: null,
        lostCampaignData: null,
        creditRequestHistoryData: null,
      });
      return newState;

    default:
      return state || newState;
  }
}
