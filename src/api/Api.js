import axios from 'axios';
import Auth from '../auth/index';
import {
  AS_USER_TOKEN,
  U_BASE,
  U_DISPUTE_CREDIT,
  U_FORGOT_PASS,
  U_GENERATE_OTP,
  U_GET_CAMPAIGN,
  U_GET_CAMPAIGN_LEADERBOARD,
  U_GET_CREDIT_HISTORY,
  U_GET_CREDIT_REQUEST_HISTORY,
  U_GET_LOST_CAMPAIGN,
  U_GET_MY_WON_GOALPRIZE,
  U_GET_NOTIFICATION,
  U_GET_PROVINCES,
  U_GET_SINGLE_CAMPAIGN,
  U_GET_USER_DETAIL,
  U_LOGIN,
  U_LOGOUT,
  U_MY_CURRENT_CAMPAIGN,
  U_REDEEM_CREDIT,
  U_REQUEST_CREDIT,
  U_RESET_PASS,
  U_SIGNUP,
  U_UPDATE_USER_DETAIL,
  U_VERIFY_OTP,
} from '../commonView/Constants';
import {showAlertMessage} from '@/commonView/Helpers';
import {Alert} from 'react-native';
import NetInfo, {useNetInfo} from '@react-native-community/netinfo';
import {showMessage} from 'react-native-flash-message';

const headers = async () => {
  const headers = {
    'Content-Type': 'application/json',
  };

  const auth = new Auth();
  const token = await auth.getValue(AS_USER_TOKEN);

  await console.log('user token is:-', token);
  if (token) {
    headers.Authorization = `Bearer ${token}`;
  }
  return headers;
};

const request = async (method, path, body) => {
  const url = `${U_BASE}${path}`;
  const options = {method, url, headers: await headers()};
  if (body) {
    options.data = body;
  }
  console.log('options are:-', options, body);

  const unsubscribe = NetInfo.addEventListener(state => {
    console.log(
      'state.isConnected ',
      state.isConnected,
      state.isInternetReachable,
      state.isWifiEnabled,
    );
    if (state.isConnected == false) {
      showMessage({
        message: 'No Internet connection',
        type: 'failure',
        style: {padding: 15},
      });
    }
  });

  unsubscribe();

  return axios(options);
};

export default class API {
  logoutUser(data) {
    return request('GET', U_LOGOUT + data);
  }

  loginAPI(data) {
    return request('POST', U_LOGIN, data);
  }

  updateUserdetail(data) {
    return request('POST', U_UPDATE_USER_DETAIL, data);
  }

  getUserDetail(data) {
    return request('GET', U_GET_USER_DETAIL);
  }

  resetPassword(data) {
    return request('POST', U_RESET_PASS, data);
  }

  signupAPI(data) {
    return request('POST', U_SIGNUP, data);
  }

  forgotPassword(data) {
    return request('POST', U_FORGOT_PASS, data);
  }

  getProvince(type) {
    return request('GET', U_GET_PROVINCES);
  }

  generateOtp(data) {
    return request('POST', U_GENERATE_OTP + data);
  }

  verifyOtp(data) {
    return request('POST', U_VERIFY_OTP, data);
  }

  requestCredit(data, type) {
    return request(
      'POST',
      type == 2 ? U_REDEEM_CREDIT : U_REQUEST_CREDIT,
      data,
    );
  }

  raiseDispute(data) {
    return request('POST', U_DISPUTE_CREDIT, data);
  }

  getMyCurrentParticipatedCampaign(data) {
    return request('GET', U_MY_CURRENT_CAMPAIGN);
  }

  getCreditRedeemHistory(data) {
    return request('POST', U_GET_CREDIT_HISTORY, data);
  }

  getCreditRequestHistory() {
    return request('GET', U_GET_CREDIT_REQUEST_HISTORY);
  }

  getUserNotification() {
    return request('GET', U_GET_NOTIFICATION);
  }

  getMyWonGoalPrize(data) {
    return request('GET', U_GET_MY_WON_GOALPRIZE);
  }

  getLostCampaign(data) {
    return request('GET', U_GET_LOST_CAMPAIGN);
  }

  getCampaign(data) {
    return request('POST', U_GET_CAMPAIGN, data);
  }

  getSingleCampaignDetail(data) {
    return request('GET', U_GET_SINGLE_CAMPAIGN + data);
  }

  getCampaignLeaderboard(data) {
    return request('GET', U_GET_CAMPAIGN_LEADERBOARD + data);
  }
}
