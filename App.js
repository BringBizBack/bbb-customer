/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import React, {useEffect} from 'react';
import {LogBox, Text, View, Image, SafeAreaView} from 'react-native';
import RootStack from './src/components/routes/Routes';
import {StatusBar} from 'react-native';
import FlashMessage from 'react-native-flash-message';
import Styles from './src/styles/Styles';
import COLOR from './src/styles/Color';
import messaging from '@react-native-firebase/messaging';
import {
  AS_APP_LANGUAGE,
  AS_FCM_TOKEN,
  K_ENGLISH_LANG,
  K_THAI_LANG,
} from './src/commonView/Constants';
import Auth from './src/auth/index';
import NotificationPopup from 'react-native-push-notification-popup';
import CampaignActions from './src/redux/action/CampaignActions';
import {connect} from 'react-redux';
import IMAGES from './src/styles/Images';
import AuthenticationAction from './src/redux/action/AuthenticationAction';
import ExploreCampaign from './src/components/campaign/ExploreCampaign';
import * as RNLocalize from 'react-native-localize';
import i18n from 'i18n-js';
import EnglishLocale from './src/components/translation/en.json';
import ThaiLocale from './src/components/translation/th.json';
import {Locale, navigate, setI18nConfig} from './src/commonView/Helpers';
import AlertPopup from './src/commonView/AlertPopup';
import NetInfo, {useNetInfo} from '@react-native-community/netinfo';

let auth = new Auth();

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      alertModalVisible: false,
      selectedLanguage: '',
      noInternet: false,
    };

    Text.defaultProps = Text.defaultProps || {};
    Text.defaultProps.allowFontScaling = false;

    this.handleLocalizationChange();

    this.closeVerifyModal = this.closeVerifyModal.bind(this);
  }

  async componentDidMount() {
    this.requestUserPermission();
    this.addFirebaseListner();

    NetInfo.addEventListener(state => {
      this.setState({
        noInternet: !state.isConnected,
      });
    });

    //  this.props.showIndicator();
    this.handleRedirection = this.handleRedirection.bind(this);
    RNLocalize.addEventListener('change', this.handleLocalizationChange);
    let appLang = await auth.getValue(AS_APP_LANGUAGE);

    // if (appLang) {
    //   console.log('applange', appLang);
    //   setI18nConfig(appLang);
    // } else {
    //   //this.setState({alertModalVisible: true});
    // }
  }

  handleLocalizationChange = () => {
    setI18nConfig();
    this.forceUpdate();
  };

  renderCustomPopup = ({appIconSource, appTitle, timeText, title, body}) => (
    <View
      style={{
        backgroundColor: 'rgba(255,255,255,0.94)',
        justifyContent: 'center',
        padding: 10,
        borderRadius: 10,
      }}>
      <View
        style={{
          flexDirection: 'row',
          height: 20,
          alignItems: 'center',
          marginBottom: 10,
        }}>
        <Image
          source={IMAGES.app_icon}
          style={{height: 15, width: 15, marginRight: 10}}
        />
        <Text style={[Styles.small_label]}>Goalprize</Text>
      </View>
      <Text style={[Styles.bold_body_label]}>{title}</Text>
      <Text style={[Styles.small_label]}>{body}</Text>
    </View>
  );

  refreshData = msg => {
    this.props.getUserNotification(1);
    switch (msg.data.notificationType) {
      //5: Add Credit (From Business End)
      //  11: New Campaign Started(Business)
      case '5':
        this.props.getCreditRedeemHistoryData(1);
        this.props.getMyParticipatedCampaign(1);
      case '11':
        this.props.getCampaignData(1);
        break;
      //6: Redeem Credit (From Business End)
      //7: Request Credit (From Customer End)
      //  8: Redeem Credit Request(From Customer End)
      //  9: Credit Approved
      case '6':
      case '7':
      case '8':
      case '9':
        this.props.getCreditRedeemHistoryData(1);
        this.props.getMyParticipatedCampaign(1);
        this.props.getMyWonGoalprize(1);
        break;
      //  10: Redeem Credit Approved
      case '10':
        this.props.getCreditRedeemHistoryData(1);
        this.props.getMyWonGoalprize(1);
        break;
      case '13':
        this.props.getMyWonGoalprize(1);
        this.props.getLostCampaignData(1);
        this.props.getMyParticipatedCampaign(1);
        break;
      default:
        break;
    }
  };

  addFirebaseListner = async () => {
    let that = this;
    messaging().onMessage(async remoteMessage => {
      console.log('remoteMessage', remoteMessage);
      if (remoteMessage && remoteMessage.data.notificationType == 5) {
        navigate('LeaderboardScreen', {
          data: remoteMessage.data,
          redirectionFromNotification: true,
          tabScreen: true,
        });
      }
      if (remoteMessage && remoteMessage.data.notificationType == 6) {
        navigate('MyTabs', {index: 0});
      }
      this.refreshData(remoteMessage);
      this.popup.show({
        onPress: function () {
          that.handleRedirection(remoteMessage);
        },
        appTitle: 'Goalprize',
        timeText: 'Now',
        val: 'called',
        title: remoteMessage
          ? remoteMessage.notification
            ? remoteMessage.notification.title
            : ''
          : '',
        body: remoteMessage
          ? remoteMessage.notification
            ? remoteMessage.notification.body
            : ''
          : '',
        slideOutTime: 3000,
      });
    });

    messaging().setBackgroundMessageHandler(async remoteMessage => {
      console.log('Message handled in the background!', remoteMessage);
    });

    messaging().onNotificationOpenedApp(remoteMessage => {
      console.log(
        'Notification caused app to open from background state:',
        remoteMessage.notification,
      );
      //Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
      // navigation.navigate(remoteMessage.data.type);
      that.refreshData(remoteMessage);
      that.handleRedirection(remoteMessage);
    });
    // Check whether an initial notification is available
    messaging()
      .getInitialNotification()
      .then(remoteMessage => {
        if (remoteMessage) {
          console.log(
            'Notification caused app to open from quit state:',
            remoteMessage.notification,
          );
          that.refreshData(remoteMessage);
          that.handleRedirection(remoteMessage);
        }
      });
  };

  requestUserPermission = async () => {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;
    await messaging().requestPermission({
      sound: true,
      alert: true,
      provisional: true,
      announcement: true,
    });

    if (enabled) {
      this.getFcmToken(); //<---- Add this
      console.log('Authorization status:', authStatus);
    } else {
      //Alert.alert('error1');
    }
  };

  getFcmToken = async () => {
    const fcmToken = await messaging().getToken();
    if (fcmToken) {
      auth.setValue(AS_FCM_TOKEN, fcmToken);
      console.log('Your Firebase Token is:', fcmToken);
    } else {
      console.log('Failed', 'No token received');
    }
  };

  handleRedirection(val) {
    switch (val.data.notificationType) {
      //5: Add Credit (From Business End)
      //6: Redeem Credit (From Business End)
      case '5':
        navigate('LeaderboardScreen', {
          data: val.data,
          redirectionFromNotification: true,
          tabScreen: true,
        });
        break;
      //  9: Credit Approved
      case '9':
      case '6':
        navigate('MyTabs', {index: 0});
        break;
      //  10: Redeem Credit Approved
      case '10':
        navigate('MyTabs', {index: 0});
        break;
      //  11: New Campaign Started
      case '11':
        navigate('ExploreCampaign', {tabScreen: true});
        break;
      //  13:  Campaign Ended(Customer)
      case '13':
        navigate('MyTabs', {index: 0});
        break;
      default:
        break;
    }
  }

  closeVerifyModal = val => {
    if (val == 3) {
      auth.setValue(AS_APP_LANGUAGE, this.state.selectedLanguage);
      setI18nConfig(this.state.selectedLanguage);
      this.setState({alertModalVisible: false});
    } else {
      this.setState({
        selectedLanguage: val == 1 ? K_ENGLISH_LANG : K_THAI_LANG,
      });
    }
  };

  render() {
    StatusBar.setBarStyle('light-content', true);
    LogBox.ignoreAllLogs();
    return (
      <View style={{flex: 1}}>
        {this.state.noInternet && (
          <SafeAreaView>
            <View
              style={{
                margin: 0,
                height: 25,
                backgroundColor: !this.state.noInternet
                  ? COLOR.GREEN
                  : COLOR.LIGHT_TEXT,

                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
                width: '100%',
              }}>
              <Text style={[Styles.extra_small_label, {color: COLOR.WHITE}]}>
                NO INTERNET
              </Text>
            </View>
          </SafeAreaView>
        )}
        <RootStack />
        <AlertPopup
          selectedLanguage={this.state.selectedLanguage}
          closeVerifyModal={this.closeVerifyModal}
          modalVisible={this.state.alertModalVisible}
          isLanguagePopup={true}
        />
        <FlashMessage
          titleStyle={(Styles.bold_body_label, {color: COLOR.WHITE})}
          floating={true}
          style={{alignItems: 'center'}}
          position="center"
        />

        <NotificationPopup
          renderPopupContent={this.renderCustomPopup}
          ref={ref => (this.popup = ref)}
        />
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getCampaignData: (val, id) => {
      dispatch(CampaignActions.getCampaignData(val, id));
    },
    getCreditRedeemHistoryData: val => {
      dispatch(CampaignActions.getCreditRedeemHistoryData(val));
    },
    getMyWonGoalprize: val => {
      dispatch(CampaignActions.getMyWonGoalprize(val));
    },
    getLostCampaignData: val => {
      dispatch(CampaignActions.getLostCampaignData(val));
    },
    getMyParticipatedCampaign: val => {
      dispatch(CampaignActions.getMyParticipatedCampaign(val));
    },
    getUserNotification: val => {
      dispatch(AuthenticationAction.getNotifications(val));
    },
  };
};

export default connect(null, mapDispatchToProps)(App);
